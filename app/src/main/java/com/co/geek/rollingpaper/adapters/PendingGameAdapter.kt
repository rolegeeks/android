package com.co.geek.rollingpaper.adapters

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.co.geek.rollingpaper.R
import com.co.geek.rollingpaper.logic.PendingGame
import com.co.geek.rollingpaper.ui.game.BaseGame
import com.co.geek.rollingpaper.ui.game.BaseGame.Companion.PARTY_ID
import com.co.geek.rollingpaper.ui.lobby.LobbyViewModel.Companion.EX_PARTY_ID
import com.co.geek.rollingpaper.ui.lobby.guestlobby.GuestLobby
import com.co.geek.rollingpaper.ui.lobby.hostlobby.HostLobby
import kotlinx.android.synthetic.main.rv_game_model.view.*

//TODO(Create a new object Game, and replace the list object with that)
class PendingGameAdapter(private var gameList: ArrayList<PendingGame>)
    : RecyclerView.Adapter<PendingGameAdapter.ViewHolder>(){

    private val onClickListener: View.OnClickListener

    init {
        onClickListener = View.OnClickListener { v ->
            val item = v.tag as PendingGame

            //TODO(Should we check if we have a game as a host? That really shouldn't happen...)
            if(!item.isRunning) {

                if(item.host) {
                    v.context.startActivity(Intent(v.context, HostLobby::class.java)
                        .putExtra(EX_PARTY_ID, item.uid))
                } else {
                    v.context.startActivity(Intent(v.context, GuestLobby::class.java)
                        .putExtra(EX_PARTY_ID, item.uid))
                }
            } else {
                v.context.startActivity(Intent(v.context, BaseGame::class.java)
                    .putExtra(PARTY_ID, item.uid))
            }
        }
    }

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val gameName: TextView = itemView.tv_game
        val gameDescription: TextView = itemView.tv_game_description
        val campaignImage: ImageView = itemView.iv_campaign
        val gameProgress: ProgressBar = itemView.pb_game
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.rv_game_model, parent, false))
    }

    override fun getItemCount(): Int {
        return gameList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(holder) {
            gameName.text = gameList[position].name
            gameDescription.text = gameList[position].description
            //TODO(Create come function for the progress object to return an int value
            //gameProgress.progress = gameList[position].progress
            itemView.tag = gameList[position]
            itemView.setOnClickListener(onClickListener)
            //TODO("Implement Glide to load image either from the device or the storage")
        }
    }

    fun updateGames(newGameList: List<PendingGame>) {
        gameList = ArrayList(newGameList)
        notifyDataSetChanged()
    }
}