package com.co.geek.rollingpaper.utils

import android.util.Log
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.Exception

object RetrofitClient {
    private var _instance: Retrofit? = null
    var httpClient: OkHttpClient? = null

    val instance : Retrofit
        get() {
            if(_instance == null) {
                httpClient = OkHttpClient.Builder()
                    .addInterceptor {

                    val request = it.request()
                    val response = it.proceed(request)

                    response
                }.build()
                try{
                    _instance = Retrofit.Builder()
                        .baseUrl("http://10.0.2.2:5001/rollingpaper-35b07/us-central1/")
//                        .baseUrl("https://us-central1-rollingpaper-35b07.cloudfunctions.net/")
                        .addConverterFactory(GsonConverterFactory
                            .create(GsonBuilder().setLenient().create()))
                        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                        .client(httpClient!!)
                        .build()
                } catch (ex: Exception) {
                    Log.e("Pepe", ex.message)
                }
            }
            return _instance!!
        }
}