package com.co.geek.rollingpaper.utils

class FirebaseConsts {

    companion object {
        //Root object
        const val GAMES = "Games"
        const val APP_SETTINGS = "AppSettings"
        const val PARTY = "Party"
        const val USERS = "Users"
        const val CHATS = "Chats"
        const val CAMPAIGNS = "Campaigns"
        const val CHARACTERS = "Characters"

        const val MEMBERS = "members"
        const val USER_PENDING_GAMES = "pendingGames"
        const val CHARACTER_LIST = "characterList"
        const val FRIENDS = "friends"

        //User Constant
        const val USER_CHARACTERS = "ownedCharacters"

        //Party Constants
        const val PARTY_CAMPAIGN = "campaign"

        //Game Constants
        const val GAME_PROGRESS = "progress"
        const val GAME_CHECKEDINMEMBERS = "checkedInMembers"
        const val GAME_BOXES = "boxes"
    }
}