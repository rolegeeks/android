package com.co.geek.rollingpaper.ui.startgroup

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.co.geek.rollingpaper.R
import com.co.geek.rollingpaper.ui.startgroup.StartGroupFragment
import com.co.geek.rollingpaper.utils.RetrofitClient
import okhttp3.OkHttpClient

class StartGroup : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.start_group_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, StartGroupFragment.newInstance())
                .commitNow()
        }
    }

}
