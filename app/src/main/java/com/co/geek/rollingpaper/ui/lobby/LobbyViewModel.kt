package com.co.geek.rollingpaper.ui.lobby

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.co.geek.rollingpaper.logic.*
import com.co.geek.rollingpaper.logic.login.FirebaseResult
import com.co.geek.rollingpaper.repository.CharacterRepository
import com.co.geek.rollingpaper.repository.MessageRepository
import com.co.geek.rollingpaper.repository.PartyRepository
import com.co.geek.rollingpaper.ui.BaseViewModel
import java.lang.Exception
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

abstract class LobbyViewModel : BaseViewModel() {
    companion object {
        const val EX_PARTY_ID = "party_id"
    }

    /**
     * States
     */
    protected var _deleteResult : MutableLiveData<FirebaseResult> = MutableLiveData()
    var deleteResult: LiveData<FirebaseResult> = _deleteResult

    protected var _joinedStatus : MutableLiveData<PartyStatus> = MutableLiveData()
    var joinedStatus: LiveData<PartyStatus> = _joinedStatus

    private var _characterSelectionStatus: MutableLiveData<FirebaseResult> = MutableLiveData()
    var characterSelectionStatus: LiveData<FirebaseResult> = _characterSelectionStatus

    protected var _uiState : MutableLiveData<FirebaseResult> = MutableLiveData()
    var uiState: LiveData<FirebaseResult> = _uiState

    protected var _updateStatus: MutableLiveData<FirebaseResult> = MutableLiveData()
    var updateStatus: LiveData<FirebaseResult> = _updateStatus

    private var _sendState: MutableLiveData<FirebaseResult> = MutableLiveData()
    var sendState: LiveData<FirebaseResult> = _sendState

    /**
     * Repositories
     */
    protected val partyRepository = PartyRepository()
    protected val characterRepository = CharacterRepository()
    private val messageRepository = MessageRepository()

    /**
     * Actual data
     */
    fun getParty(partyId: String) : LiveData<Party> {
        return partyRepository.getSingle(partyId, _uiState)
    }

    fun getCharacters() : LiveData<List<Character>> {
        return characterRepository.getAll()
    }

    fun getChatMessages(partyId: String) : LiveData<Message> {
        return messageRepository.getSingle(partyId)
    }

    fun sendChatMessage(partyId: String, text: String, userName: String) {
        val newMessage = Message()
        newMessage.uid = partyId
        newMessage.message = text
        newMessage.displayName = userName
        val time = LocalDateTime.now()
        newMessage.timestamp = time.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))
        messageRepository.add(newMessage, _sendState)
    }

    fun updateCharacterSelection(partyId: String, characterId: String) {
        try {
            //null checks?
            val party = partyRepository.getUnit()

            val canUpdate = party.members.firstOrNull { member -> member.characterId == characterId }

            if(canUpdate != null) {
                _characterSelectionStatus.value = FirebaseResult(
                    state = FirebaseState.Failed,
                    message = "Character already selected, pick another one"
                )
            } else {
                partyRepository.updateCharacterSelection(partyId, characterId, _characterSelectionStatus)
            }
        } catch (ex: Exception) {
            _characterSelectionStatus.value = FirebaseResult(
                state = FirebaseState.Failed,
                message = ex.message
            )
        }
    }

    override fun clearListeners() {
        partyRepository.cleanUpListeners()
        messageRepository.cleanUpListeners()
    }

    abstract fun updateStatus(partyId: String)
    abstract fun updateJoinedStatus(partyId: String, joined: Boolean)
    abstract fun deleteParty()
    abstract fun startGroup()
}