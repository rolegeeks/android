package com.co.geek.rollingpaper.ui.login

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.lifecycle.Observer
import android.os.Bundle
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider

import com.co.geek.rollingpaper.R
import com.co.geek.rollingpaper.logic.FirebaseState
import com.co.geek.rollingpaper.ui.ViewModelFactory
import com.co.geek.rollingpaper.ui.main.MainActivity
import com.co.geek.rollingpaper.utils.Global
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_login.*
import java.lang.Exception

class LoginActivity : AppCompatActivity() {

    private val TAG = LoginActivity::class.java.name

    private lateinit var loginViewModel: LoginViewModel
    private lateinit var auth: FirebaseAuth
    private lateinit var signInClient: GoogleSignInClient

    companion object {
        private const val RC_SIGN_IN = 9001
        private const val SP_LOGIN = "login"
        private const val SP_EMAIL = "email"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        Thread.sleep(1000)
        setTheme(R.style.AppTheme)

        createNotificationChannel()

        if(Firebase.auth.currentUser != null) {
            startActivity(Intent(this, MainActivity::class.java).apply {
                this.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            })
            finish()
        }

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        auth = FirebaseAuth.getInstance()

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()

        signInClient = GoogleSignIn.getClient(this, gso)

        val username = findViewById<EditText>(R.id.username)
        val login = findViewById<Button>(R.id.login)
        val loading = findViewById<ProgressBar>(R.id.loading)

        loginViewModel = ViewModelProvider(this, ViewModelFactory(applicationContext))
            .get(LoginViewModel::class.java)

        loginViewModel.loginFormState.observe(this@LoginActivity, Observer {
            val loginState = it ?: return@Observer

            // disable login button unless both username / password is valid
            login.isEnabled = loginState.isDataValid

            if (loginState.usernameError != null) {
                username.error = getString(loginState.usernameError)
            }
        })

        loginViewModel.loginResult.observe(this@LoginActivity, Observer {
            val loginResult = it ?: return@Observer

            loading.visibility = View.GONE
            when(loginResult.state) {
                LoginState.failed -> {
                    if (loginResult.error != null) {
                        showLoginFailed(loginResult.error)
                    }
                }
                LoginState.waitingForEmail -> {
                    if (loginResult.userView != null) {
                        showToast(loginResult.userView)
                    }
                }
                LoginState.succeeded -> {
                    if (loginResult.userView != null) {
                        startActivity(Intent(this, MainActivity::class.java).apply {
                            this.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                        })
                    }
                }
            }
        })

        loginViewModel.firebaseResult.observe(this@LoginActivity, Observer {
            val firebaseResult = it ?: return@Observer

            loading.visibility = View.GONE

            when(firebaseResult.state) {
                FirebaseState.Nothing -> TODO()
                FirebaseState.Succeeded -> {
                    startActivity(Intent(this, MainActivity::class.java).apply {
                        this.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    })
                }
                FirebaseState.Failed -> {
                    Snackbar.make(container, firebaseResult.message!!, Snackbar.LENGTH_INDEFINITE)
                        .setAction("OK"){}.show()
                }
                FirebaseState.Cancelled -> TODO()
            }
        })

        username.afterTextChanged {
            loginViewModel.loginDataChanged(
                username.text.toString()
            )
        }

        login.setOnClickListener {
            loading.visibility = View.VISIBLE

            //saving email for later use
            getSharedPreferences(SP_LOGIN, Context.MODE_PRIVATE)
                .edit()
                .putString(SP_EMAIL, username.text.toString())
                .apply()

            //performing login
            loginViewModel.login(username.text.toString())
        }

        if(intent.extras != null) {
            try {
                val savedEmail = getSharedPreferences(SP_LOGIN, Context.MODE_PRIVATE)
                    .getString(SP_EMAIL, "")
                loginViewModel.login(intent.data!!.toString(), savedEmail)
            } catch (ex: Exception) {
                Log.e(TAG,"Error retrieving intent ${ex.message} stack: ${ex.printStackTrace()}")
            }

        }

        google_button.setOnClickListener{
            val signInIntent = signInClient.signInIntent
            startActivityForResult(signInIntent, RC_SIGN_IN)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        loginViewModel.clearListeners()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                // Google Sign In was successful, authenticate with Firebase
                val account = task.getResult(ApiException::class.java)
                loginViewModel.login(account!!)
            } catch (e: ApiException) {
                // Google Sign In failed, update UI appropriately
                Log.w(TAG, "Google sign in failed", e)
                // ...
            }
        }
    }

    private fun showToast(model: LoggedInUserView) {
        val welcome = getString(R.string.welcome)
        val displayName = model.displayName
        // TODO : initiate successful logged in experience
        Toast.makeText(
            applicationContext,
            "$welcome $displayName",
            Toast.LENGTH_LONG
        ).show()
    }

    private fun showLoginFailed(@StringRes errorString: Int) {
        Snackbar.make(container, errorString, Snackbar.LENGTH_INDEFINITE).setAction("OK"){}.show()
    }

    //TODO("move this into a helper class")
    private fun createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = getString(R.string.channel_invitation)
            val descriptionText = getString(R.string.channel_invitation_description)
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(Global.INVITATION_CHANNEL, name, importance).apply {
                description = descriptionText
            }
            // Register the channel with the system
            val notificationManager: NotificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }
}

/**
 * Extension function to simplify setting an afterTextChanged action to EditText components.
 */
fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(editable: Editable?) {
            afterTextChanged.invoke(editable.toString())
        }

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
    })
}
