package com.co.geek.rollingpaper.ui.gameviewer

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.co.geek.rollingpaper.logic.Game
import com.co.geek.rollingpaper.logic.PendingGame
import com.co.geek.rollingpaper.repository.GameRepository
import com.co.geek.rollingpaper.repository.PartyRepository
import com.co.geek.rollingpaper.ui.BaseViewModel

/**
 * Here we should house the logic obtained parties that can be resumed and games that are on hold
 */
class GameViewerViewModel() : BaseViewModel() {

    private var gameRepository : GameRepository = GameRepository(null, "")

    fun getPendingParties() : LiveData<List<PendingGame>> {
        return gameRepository.getPendingGames()
    }

    override fun clearListeners() {
        gameRepository.cleanUpListeners()
    }
}