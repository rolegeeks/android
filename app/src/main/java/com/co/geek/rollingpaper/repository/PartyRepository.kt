package com.co.geek.rollingpaper.repository

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.co.geek.rollingpaper.`interface`.ICloudMessaging
import com.co.geek.rollingpaper.logic.*
import com.co.geek.rollingpaper.logic.login.FirebaseResult
import com.co.geek.rollingpaper.utils.FirebaseConsts.Companion.USER_PENDING_GAMES
import com.co.geek.rollingpaper.utils.FirebaseConsts.Companion.MEMBERS
import com.co.geek.rollingpaper.utils.FirebaseConsts.Companion.PARTY
import com.co.geek.rollingpaper.utils.RetrofitClient
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.ktx.Firebase
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception

class PartyRepository : BaseRepository<Party>() {

    private val TAG = PartyRepository::class.java.name
    private var userState: Boolean = false
    private var cloudMessagingAPI: ICloudMessaging = RetrofitClient.instance.create(
        ICloudMessaging::class.java)

    override fun getAll(
        uid: String?,
        listener: MutableLiveData<FirebaseResult>?
    ): MutableLiveData<List<Party>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getSingle(
        uid: String?,
        listener: MutableLiveData<FirebaseResult>?
    ): MutableLiveData<Party> {

        val eventListener = database.child(PARTY).child(uid!!)
            .addValueEventListener(object : ValueEventListener {

            override fun onCancelled(p0: DatabaseError) {
                single.value = null
            }

            override fun onDataChange(p0: DataSnapshot) {
                try {
                    if(p0.value == null) {
                        single.value = null

                        listener!!.value = FirebaseResult(
                            state = FirebaseState.Removed,
                            message = "Party has been dismissed"
                        )
                    } else {
                        val newParty = Party(p0)

                        val user = newParty.members.first { x -> x.uid == Firebase.auth.uid}
                        userState = user.ready

                        var enableUI = true
                        if(user.host) {
                            enableUI = newParty.members
                                .all { user ->
                                    with(user) {
                                        host && characterId.isNotEmpty()
                                                || ready && inParty && !host && characterId.isNotEmpty()
                                    }
                                }
                        }

                        if(listener != null) {
                            listener.value = FirebaseResult(
                                state = FirebaseState.Succeeded,
                                enableUI = enableUI
                            )
                        }
                        single.value = newParty
                    }
                }
                catch (ex: Exception) {
                    Log.e(TAG, "Something went wrong retrieving the party ${ex.message}")

                    if(listener != null) {
                        listener.value = FirebaseResult(
                            state = FirebaseState.Failed
                        )
                    }
                }
            }

        })

        trackListener(eventListener)

        return single
    }

    override fun add(item: Party, listener: MutableLiveData<FirebaseResult>?) {
        val ref = database.child(PARTY)
        val id = ref.push().key

        ref.child(id!!).setValue(item.toFirebaseMap())
            .addOnSuccessListener {
                item.members.forEach {
                    database
                        .child("Users/${it.uid}/$USER_PENDING_GAMES/$id")
                        .setValue(id).addOnFailureListener { ex ->
                        Log.e(TAG, "log it somehow, ${ex.message}")
                    }
                }
                item.members.forEach {
                    if(it.token != null && !it.host) {
                        sendInvitation(it.token!!, id)
                    }
                }

                listener?.value = FirebaseResult(
                    state = FirebaseState.Succeeded,
                    message = "Party created",
                    enableUI = true,
                    uid = id)
            }
            .addOnFailureListener {
                Log.e(TAG, "Error creating party ${it.message}}")
                listener?.value = FirebaseResult(
                    state = FirebaseState.Failed,
                    message = "Failed to create party because: ${it.message}",
                    enableUI = true
                    )
            }
    }

    override fun delete(item: Party, listener: MutableLiveData<FirebaseResult>?) {
        database.child(PARTY).child(item.uid).removeValue()
            .addOnSuccessListener {
                item.members.forEach {
                    deleteUserParty(item.uid, it.uid)
                }
                listener?.value = FirebaseResult(
                    state = FirebaseState.Succeeded,
                    message = "Party deleted successfully"
                )
            }
            .addOnFailureListener {
                Log.e(TAG, "Something went wrong deleting the party ${it.message}")
                listener?.value = FirebaseResult(
                    state = FirebaseState.Failed,
                    message = "Error deleting the party"
                )
            }
    }

    private fun deleteUserParty(uid: String, userId: String) {
        database
            .child("Users/${userId}/$USER_PENDING_GAMES/${uid}")
            .removeValue().addOnFailureListener { ex ->
                Log.e(TAG, "log it somehow, ${ex.message}")
            }
    }

    fun updateStatus(partyId: String, listener: MutableLiveData<FirebaseResult>) {
        database
            .child("Party/$partyId")
            .child(MEMBERS)
            .child(Firebase.auth.uid!!)
            .child("ready")
            .setValue(!userState)
            .addOnSuccessListener {
                listener.value = FirebaseResult(
                    state = FirebaseState.Succeeded
                )
            }
            .addOnFailureListener {
                val errorMessage = "error updating status ${it.message}"
                Log.e(TAG, errorMessage)
                listener.value = FirebaseResult(
                    state = FirebaseState.Failed,
                    message = errorMessage
                )
            }
    }

    fun updateJoinedStatus(partyId: String, joined: Boolean, listener: MutableLiveData<PartyStatus>?) {
        database
            .child(PARTY)
            .child(partyId)
            .child(MEMBERS)
            .child(Firebase.auth.uid!!)
            .child("inParty")
            .setValue(joined)
            .addOnSuccessListener {
                if(listener != null) {
                    listener.value = PartyStatus(
                        state = if (joined) PartyState.Joined else PartyState.Left
                    )
                }
            }
            .addOnFailureListener {
                val errorMessage = "error updating joined status ${it.message}"
                if(listener != null) {
                    listener.value = PartyStatus(
                        state = PartyState.Error,
                        message = errorMessage
                    )
                }
            }
    }

    fun startGroup(party: Party, listener: MutableLiveData<FirebaseResult>) {
        cloudMessagingAPI.createGame(party).enqueue(object : Callback<String> {
            override fun onFailure(call: Call<String>, t: Throwable) {
                Log.e(TAG, t.message)
                listener.value = FirebaseResult(
                    state = FirebaseState.Failed,
                    message = t.message
                )
            }

            override fun onResponse(call: Call<String>, response: Response<String>) {
                if (response.code() != 200) {
                    Log.e(TAG, response.message())
                    listener.value = FirebaseResult(
                        state = FirebaseState.Failed,
                        message = response.message()
                    )
                }
            }

        })
    }

    fun updateCharacterSelection(partyId: String, characterId: String, listener: MutableLiveData<FirebaseResult>) {

        database
            .child(PARTY)
            .child(partyId)
            .child(MEMBERS)
            .child(Firebase.auth.uid!!)
            .child("characterId")
            .setValue(characterId)
            .addOnSuccessListener {

                listener.value = FirebaseResult(
                    state = FirebaseState.Succeeded
                )
            }
            .addOnFailureListener {

                listener.value = FirebaseResult(
                    state = FirebaseState.Failed,
                    message = it.message
                )
            }
    }

    private fun sendInvitation(token: String, partyToken: String) {
        cloudMessagingAPI.sendInvitation(token, partyToken).enqueue(object :
            Callback<String> {
            override fun onFailure(call: Call<String>, t: Throwable) {
                Log.e(TAG, t.message)
            }

            override fun onResponse(call: Call<String>, response: Response<String>) {
                Log.d(TAG, response.message())
            }

        })
    }
}