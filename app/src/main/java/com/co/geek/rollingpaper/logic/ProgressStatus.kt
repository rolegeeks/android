package com.co.geek.rollingpaper.logic

enum class ProgressStatus {
    Undefined,
    WaitingForPlayers,
    Synced,
    Advance,
    ActionTaken,
    Error
}