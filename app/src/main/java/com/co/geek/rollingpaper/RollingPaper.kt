package com.co.geek.rollingpaper

import android.app.Application
import android.content.Context
import android.os.Environment
import org.acra.ACRA
import org.acra.config.CoreConfigurationBuilder
import org.acra.config.DialogConfigurationBuilder
import org.acra.config.MailSenderConfigurationBuilder
import org.acra.data.StringFormat
import java.io.File
import java.io.IOException
import java.lang.Exception
import java.util.*
import kotlin.collections.ArrayList
import kotlin.time.days


open class RollingPaper : Application() {

    override fun onCreate() {
        super.onCreate()

        if ( isExternalStorageWritable() ) {
            val time = Calendar.getInstance().time
            var date = "${time.day}-${time.month}-${time.year + 1900}"
            val appDirectory = File( "${Environment.getExternalStorageDirectory()}/RollingPaper" );
            val logDirectory = File( "${appDirectory}/log" );
            val logFile = File( logDirectory, "logcat$date.log");

            // create app folder
            if ( !appDirectory.exists() ) {
                appDirectory.mkdir();
            }

            // create log folder
            if ( !logDirectory.exists() ) {
                logDirectory.mkdir();
            }

            try {
                //Here delete the last 3 days of files
                logDirectory.listFiles().forEach {
                    if(!it.name.contains(date))
                    {
                        it.delete()
                    }
                }

            } catch ( ex: Exception) {
                ex.printStackTrace()
            }

            // clear the previous logcat and then write the new one to the file
            try {
                var process = Runtime.getRuntime().exec("logcat -c");
                process = Runtime.getRuntime().exec("logcat -f $logFile");
            } catch ( e: IOException) {
                e.printStackTrace();
            }

        } else if ( isExternalStorageReadable() ) {
            // only readable
        } else {
            // not accessible
        }
    }

    private fun isExternalStorageReadable() : Boolean {
        val state = Environment.getExternalStorageState()
        return Environment.MEDIA_MOUNTED == state
                || Environment.MEDIA_MOUNTED_READ_ONLY == state
    }

    private fun isExternalStorageWritable() : Boolean {
        val state = Environment.getExternalStorageState()

        return Environment.MEDIA_MOUNTED == state
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)

        val builder = CoreConfigurationBuilder(this)
        builder.setBuildConfigClass(BuildConfig::class.java).setReportFormat(StringFormat.JSON)
        builder.getPluginConfigurationBuilder(DialogConfigurationBuilder::class.java)
            .setCommentPrompt("Would you like to send crash logs?")
            .setPositiveButtonText("Hell yes!")
            .setNegativeButtonText("GTFO stalker")
            .setText("Logs will be sent via email to the development team")
            //.setEnabled(true)
        builder.getPluginConfigurationBuilder(MailSenderConfigurationBuilder::class.java)
            .setReportAsFile(true)
            .setReportFileName("RollingPaper.log")
            .setMailTo("mauro.afa91@gmail.com")
            .setSubject("Bug report")
            .setEnabled(true)
//        ACRA.init(this, builder)
    }
}