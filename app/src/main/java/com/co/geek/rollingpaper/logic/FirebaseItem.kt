package com.co.geek.rollingpaper.logic

import com.google.firebase.database.Exclude
import com.google.firebase.database.PropertyName
import java.io.Serializable

abstract class FirebaseItem : Serializable {
    @get:Exclude
    var uid: String = ""

    fun listToMap(list: List<FirebaseItem>) : HashMap<String, Any> {
        val map = HashMap<String, Any>()
        var id = 0
        list.forEach {
            if(it.uid.isEmpty()) {
                it.uid = id.toString()
                id++
            }
            map[it.uid] = it
        }

        return map
    }

    abstract fun toFirebaseMap() : HashMap<String, Any>
}