package com.co.geek.rollingpaper.logic

import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.ktx.Firebase
import java.io.Serializable

class PendingGame(): FirebaseItem(), Serializable {

    var description = ""
    var name = ""
    var progress = GameProgress()
    var host = false
    var isRunning = false

    constructor(key: String) : this() {
        uid = key
    }

    fun fillGameFromParty(dataSnapshot: DataSnapshot) {
        with(dataSnapshot) {
            if(exists()) {
                description = dataSnapshot.child("campaign").child("description").value.toString()
                fillCommonFields(dataSnapshot)
            }
        }
    }

    fun fillGame(dataSnapshot: DataSnapshot) {
        with(dataSnapshot) {
            if (exists()) {
                description = dataSnapshot.child("description").value.toString()
                fillCommonFields(dataSnapshot)
            }
        }
    }

    private fun fillCommonFields(dataSnapshot: DataSnapshot) {
        with(dataSnapshot) {
            name = dataSnapshot.child("name").value.toString()
            progress = GameProgress(child("Progress"))
            child("members").children.forEach {
                if(it.key == Firebase.auth.uid) {
                    host = it.child("host").value as Boolean
                }
            }
        }
    }

    override fun toFirebaseMap(): HashMap<String, Any> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}