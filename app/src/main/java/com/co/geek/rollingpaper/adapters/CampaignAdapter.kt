package com.co.geek.rollingpaper.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.co.geek.rollingpaper.R
import com.co.geek.rollingpaper.logic.Campaign
import kotlinx.android.synthetic.main.campaign_model_adapter.view.*

class CampaignAdapter(var _campaignList: List<Campaign>)
    : RecyclerView.Adapter<CampaignAdapter.ViewHolder>() {

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val campaignName: TextView = itemView.tv_campaign_name
        val campaignDuration: TextView = itemView.tv_campaign_duration
        val campaignDescription: TextView = itemView.tv_campaign_description
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater
                .from(parent.context)
                .inflate(R.layout.campaign_model_adapter, parent, false))
    }

    override fun getItemCount(): Int {
        return _campaignList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.campaignName.text = _campaignList[position].name
        holder.campaignDuration.text = _campaignList[position].length.toString()
        holder.campaignDescription.text = _campaignList[position].description
    }
}