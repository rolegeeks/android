package com.co.geek.rollingpaper.logic

import com.google.firebase.database.DataSnapshot
import java.io.Serializable

class GameProgress(): Serializable {

    var boxId = 0
    var completed = false
    var checkedInMembers = arrayListOf<String>()

    constructor(dataSnapshot: DataSnapshot) : this() {

        with(dataSnapshot) {
            if(exists()) {
                boxId = child("boxId").value.toString().toInt()
                completed = child("completed").value as Boolean

                //TODO("Maybe we need to check if we have this, otherwise it could break")
                child("checkedInMembers").children.forEach {
                    checkedInMembers.add(it.value.toString())
                }
            }
        }
    }
}