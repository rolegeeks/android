package com.co.geek.rollingpaper.ui.login

/**
 * Authentication result : success (user details) or error message.
 */
data class LoginResult(
    val userView: LoggedInUserView? = null,
    val error: Int? = null,
    val state: LoginState
)

enum class LoginState {
    failed,
    waitingForEmail,
    succeeded
}
