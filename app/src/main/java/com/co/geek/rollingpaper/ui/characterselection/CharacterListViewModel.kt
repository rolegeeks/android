package com.co.geek.rollingpaper.ui.characterselection

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.co.geek.rollingpaper.logic.Character
import com.co.geek.rollingpaper.repository.CharacterRepository
import com.co.geek.rollingpaper.ui.BaseViewModel
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

class CharacterListViewModel: BaseViewModel() {

    private var characterRepository = CharacterRepository()

    fun getUserCharacters() : LiveData<List<Character>> {
        return characterRepository.getAll(Firebase.auth.uid)
    }

    override fun clearListeners() {
        characterRepository.cleanUpListeners()
    }
}