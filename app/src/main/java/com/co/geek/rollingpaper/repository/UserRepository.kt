package com.co.geek.rollingpaper.repository

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.co.geek.rollingpaper.logic.FirebaseState
import com.co.geek.rollingpaper.logic.Friend
import com.co.geek.rollingpaper.logic.User
import com.co.geek.rollingpaper.logic.login.FirebaseResult
import com.co.geek.rollingpaper.ui.login.LoginResult
import com.co.geek.rollingpaper.ui.login.LoginState
import com.co.geek.rollingpaper.ui.usernameselection.AliasFormState
import com.co.geek.rollingpaper.ui.usernameselection.AliasState
import com.co.geek.rollingpaper.utils.FirebaseConsts
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.*
import com.co.geek.rollingpaper.R
import com.co.geek.rollingpaper.logic.UserSettings
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.ktx.Firebase
import java.lang.Exception

class UserRepository(private val context: Context) : BaseRepository<User>() {

    private val TAG = UserRepository::class.java.name
    private var friendCount = 0

    var aliases: List<String> = listOf()

    override fun getAll(
        uid: String?,
        listener: MutableLiveData<FirebaseResult>?
    ): MutableLiveData<List<User>> {
        try {
            val eventListener = database.child(FirebaseConsts.USERS)
                .addValueEventListener(object : ValueEventListener {
                    override fun onCancelled(p0: DatabaseError) {
                        if(listener != null) {
                            listener.value = FirebaseResult(
                                state = FirebaseState.Cancelled,
                                message = "Cancelled all users request"
                            )
                        }
                    }

                    override fun onDataChange(p0: DataSnapshot) {
                        val userList = arrayListOf<User>()
                        p0.children.forEach {
                            val user = it.getValue(User::class.java)!!
                            user.uid = it.key!!
                            userList.add(user)
                        }

                        list.value = userList

                        if(listener != null) {
                            listener.value = FirebaseResult(
                                state = FirebaseState.Succeeded,
                                message = "Users obtained correctly"
                            )
                        }
                    }

                })
            trackListener(eventListener)
        } catch (ex: Exception) {
            if(listener != null) {
                listener.value = FirebaseResult(
                    state = FirebaseState.Failed,
                    message = "Exception caught getting all users, ex: ${ex.message}"
                )
            }
        }
        return list
   }

    override fun add(item: User, listener: MutableLiveData<FirebaseResult>?) {
        try {
            database
                .child(FirebaseConsts.APP_SETTINGS)
                .addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onCancelled(p0: DatabaseError) {
                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                    }

                    override fun onDataChange(p0: DataSnapshot) {

                        if(validateAppVersion(p0)) {
                            database.child("Users").child(item.uid)
                                .addListenerForSingleValueEvent(object: ValueEventListener {
                                    override fun onCancelled(p0: DatabaseError) {
                                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                                    }

                                    override fun onDataChange(p0: DataSnapshot) {
                                        //Move on :)
                                        if(p0.exists()) {

                                            database
                                                .child(FirebaseConsts.USERS)
                                                .child(item.uid)
                                                .child("token")
                                                .setValue(item.token)
                                                .addOnSuccessListener {
                                                    listener?.value = FirebaseResult(
                                                        state = FirebaseState.Succeeded,
                                                        message = "User updated",
                                                        enableUI = true)
                                                }
                                                .addOnFailureListener {
                                                    Log.e(TAG, "Error creating user")
                                                    listener?.value = FirebaseResult(
                                                        state = FirebaseState.Failed,
                                                        message = "Error creating user",
                                                        enableUI = true)
                                                }
                                        } else {
                                            createUser(item, listener)
                                        }
                                    }
                                })
                        } else {
                            listener?.value = FirebaseResult(
                                state = FirebaseState.Failed,
                                message = "App version not supported, please update application"
                            )
                        }
                    }
                })

        } catch (ex: Exception) {
            Log.e(TAG, "Something went wrong creating user: ${ex.message}")
        }
    }

    override fun delete(item: User, listener: MutableLiveData<FirebaseResult>?) {
        TODO("Not yet implemented")
    }

    override fun getSingle(
        uid: String?,
        listener: MutableLiveData<FirebaseResult>?
    ): MutableLiveData<User> {
        //First we retrieve the appVersion
        val eventListener = database
            .child(FirebaseConsts.APP_SETTINGS)
            .addValueEventListener(object : ValueEventListener {

                override fun onCancelled(p0: DatabaseError) {
                    single.value = null
                }

                override fun onDataChange(p0: DataSnapshot) {

                    if(validateAppVersion(p0)) {
                        database.child("${FirebaseConsts.USERS}/$uid")
                            .addListenerForSingleValueEvent(object : ValueEventListener {
                                override fun onCancelled(p0: DatabaseError) {
                                    single.value = null
                                }

                                override fun onDataChange(p0: DataSnapshot) {
                                    try {
                                        val newUser = p0.getValue(User::class.java)
                                        newUser!!.uid = p0.key!!
                                        single.value = newUser

                                        listener?.value = FirebaseResult(
                                            state = FirebaseState.Succeeded,
                                            enableUI = true
                                        )
                                    } catch (ex: Exception) {
                                        Log.e(TAG, "Clear error retrieving single user ${ex.message}")
                                        single.value = null
                                    }
                                }

                            })
                    } else {
                        listener?.value = FirebaseResult(
                            state = FirebaseState.Failed,
                            message = "App version not supported, please update application",
                            enableUI = false
                        )
                    }
                }

            })
        trackListener(eventListener)

        return single
    }

    fun login(email: String, originalEmail: String, listener: MutableLiveData<LoginResult>,
              firebaseResult: MutableLiveData<FirebaseResult>) {
        database
            .child(FirebaseConsts.APP_SETTINGS)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {
                    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }

                override fun onDataChange(p0: DataSnapshot) {

                    if(validateAppVersion(p0)) {
                        try {
                            //need to retrieve previously used password, how?
                            Firebase.auth
                                .signInWithEmailLink(originalEmail, email)
                                .addOnCompleteListener {
                                    Log.d(TAG, it.result.toString())
                                    initializeUser(firebaseResult)
                                }
                                .addOnFailureListener {
                                    Log.d(TAG, it.message)
                                    listener.value = LoginResult(
                                        error = R.string.login_failed,
                                        state = LoginState.failed)
                                }
                        } catch (e: Throwable) {

                        }
                    } else {
                        listener.value = LoginResult(
                            error = R.string.unauthorized_login,
                            state = LoginState.failed)
                    }
                }

            })
    }

    fun login(acct: GoogleSignInAccount, listener: MutableLiveData<LoginResult>,
              firebaseResult: MutableLiveData<FirebaseResult>) {
        database
            .child(FirebaseConsts.APP_SETTINGS)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {
                    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }

                override fun onDataChange(p0: DataSnapshot) {

                    if(validateAppVersion(p0)) {

                        val credential = GoogleAuthProvider.getCredential(acct.idToken, null)
                        FirebaseAuth.getInstance().signInWithCredential(credential)
                            .addOnSuccessListener {
                                Log.i(TAG, "Succeeded, welcome ${it.user?.displayName}")
                                initializeUser(firebaseResult)
                            }
                            .addOnFailureListener {
                                Log.e(TAG, it.message)
                                listener.value = LoginResult(
                                    error = R.string.login_failed,
                                    state = LoginState.failed)
                            }
                    } else {
                        listener.value = LoginResult(
                            error = R.string.unauthorized_login,
                            state = LoginState.failed)
                    }
                }

            })
    }

    private fun initializeUser(listener: MutableLiveData<FirebaseResult>?) {

        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener {
                if(!it.isSuccessful) {
                    Log.e(TAG, "getting instance failed")
                    return@addOnCompleteListener
                }
                val user = FirebaseAuth.getInstance().currentUser
                val newUser = UserSettings()
                newUser.uid = user!!.uid
                newUser.token = it.result?.token
                add(newUser, listener)
            }
            .addOnFailureListener {
                Log.e(TAG, "Something failed retrieving the instance")
            }
    }

    fun getAliases(aliasForm: MutableLiveData<AliasFormState>) {

        val eventListener = database.child(FirebaseConsts.USERS)
            .addValueEventListener(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {
                    aliasForm.value = AliasFormState(
                        AliasState.Failed,
                        message = p0.message
                    )
                }

                override fun onDataChange(p0: DataSnapshot) {
                    try {
                        val output = p0.children.map { snap -> snap.child("alias").value as String}
                        aliases = output
                        aliasForm.value = AliasFormState(
                            AliasState.Loaded
                        )
                    } catch (ex: Exception) {
                        aliasForm.value = AliasFormState(
                            AliasState.Failed,
                            message = "connection failed"
                        )
                    }
                }
            })

        trackListener(eventListener)
    }

    fun setAlias(aliasForm: MutableLiveData<AliasFormState>, alias: String) {
        //Need to properly update user profile with alias

        database
            .child(FirebaseConsts.USERS)
            .child(Firebase.auth.uid!!)
            .child("alias")
            .setValue(alias)
            .addOnCompleteListener {
                aliasForm.value = AliasFormState(
                    state = AliasState.Successful
                )
            }
            .addOnFailureListener {
                aliasForm.value = AliasFormState(
                    state = AliasState.Failed,
                    message = it.message
                )
            }
    }

    private fun createUser(item: User, listener: MutableLiveData<FirebaseResult>?) {

        database.child(FirebaseConsts.USERS).child(item.uid).setValue(item)
            .addOnSuccessListener {
                //we still have it
                listener?.value = FirebaseResult(
                    state = FirebaseState.Succeeded,
                    message = "User created",
                    enableUI = true
                )
            }
            .addOnSuccessListener {
                Log.e(TAG, "Error creating user")
            }
    }

    fun getFriends() : MutableLiveData<List<Friend>> {
        val uid = FirebaseAuth.getInstance().uid
        val friends: MutableLiveData<List<Friend>> = MutableLiveData()
        //Here we all users
        val eventListener = database.child(FirebaseConsts.USERS)
            .addValueEventListener(object : ValueEventListener {

                override fun onCancelled(p0: DatabaseError) {
                    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }

                override fun onDataChange(p0: DataSnapshot) {
                    val tempFriend : ArrayList<Friend> = ArrayList()
                    val friendIds: ArrayList<String> = ArrayList()
                    try {
                        p0.children.forEach {
                            val user = Friend()
                            user.alias = it.child("alias").value as String
                            user.token = it.child("token").value as String?
                            user.uid = it.key!!
                            tempFriend.add(user)

                            if(user.uid == uid) {

                                it.child(FirebaseConsts.FRIENDS).children.forEach{ id ->
                                    friendIds.add(id.getValue(String::class.java)!!)
                                }
                            }
                        }
                        friends.value = tempFriend.filter { user -> friendIds.contains(user.uid) }
                        friendCount = friends.value!!.count()
                    } catch (ex: Exception) {
                        Log.e(TAG, "Error retrieving friend list, ${ex.message}")
                        friends.value = arrayListOf()
                    }

                }

            })

        trackListener(eventListener)

        return friends
    }

    fun addFriend(friendId: String, alias: String, listener: MutableLiveData<FirebaseResult>?) {
        database
            .child(FirebaseConsts.USERS)
            .child(Firebase.auth.uid!!)
            .child(FirebaseConsts.FRIENDS)
            .child(friendId)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {
                    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }

                override fun onDataChange(p0: DataSnapshot) {
                    if(p0.exists()) {
                        listener?.value = FirebaseResult(
                            state = FirebaseState.Failed,
                            message = "Already have that friend",
                            enableUI = true)
                    } else {
                        createFriend(friendId, alias, listener)
                    }
                }

            })
    }

    private fun createFriend(friendId: String, alias: String, listener: MutableLiveData<FirebaseResult>?) {
        database
            .child(FirebaseConsts.USERS)
            .child(Firebase.auth.uid!!)
            .child(FirebaseConsts.FRIENDS)
            .child(friendId)
            .setValue(friendId)
            .addOnCompleteListener {
                if(listener != null) {
                    listener.value = FirebaseResult(
                        state = FirebaseState.Succeeded,
                        message = "New friend $alias added"
                    )
                }
            }
            .addOnFailureListener {
                Log.e(TAG, "Error adding user with alias: $alias")
            }
    }

    private fun validateAppVersion(snapshot: DataSnapshot): Boolean {

        return if(snapshot.exists()) {
            val versionList: ArrayList<String> =
                snapshot.child("appVersionAllowed").value as ArrayList<String>
            val appVersion = context.packageManager!!
                .getPackageInfo(context.packageName, 0).versionName
            versionList.any { v -> v == appVersion }
        } else {
            false
        }
    }
}