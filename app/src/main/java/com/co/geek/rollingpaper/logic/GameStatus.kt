package com.co.geek.rollingpaper.logic

data class GameStatus (
    val progressState : ProgressStatus,
    val currentBox: Box? = null,
    val message: String? = null
)