package com.co.geek.rollingpaper.ui.login

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import android.util.Patterns
import com.co.geek.rollingpaper.logic.login.Result

import com.co.geek.rollingpaper.R
import com.co.geek.rollingpaper.logic.UserSettings
import com.co.geek.rollingpaper.logic.login.FirebaseResult
import com.co.geek.rollingpaper.repository.UserRepository
import com.co.geek.rollingpaper.ui.BaseViewModel
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.firebase.auth.ActionCodeSettings
import com.google.firebase.auth.FirebaseAuth
import java.io.IOException

class LoginViewModel(context: Context) : BaseViewModel() {

    private val TAG = LoginViewModel::class.java.name

    private val userRepository: UserRepository = UserRepository(context)

    private val _loginForm = MutableLiveData<LoginFormState>()
    val loginFormState: LiveData<LoginFormState> = _loginForm

    private val _loginResult = MutableLiveData<LoginResult>()
    val loginResult: LiveData<LoginResult> = _loginResult

    private val _firebaseResult = MutableLiveData<FirebaseResult>()
    val firebaseResult: LiveData<FirebaseResult> = _firebaseResult

    fun login(username: String) {

        try {
            val actionCodeSettings = ActionCodeSettings.newBuilder()
                .setUrl("https://www.example.com/finishSignUp?cartId=1234")
                .setHandleCodeInApp(true)
                .setAndroidPackageName(
                    "com.co.geek.rollingpaper",
                    true,
                    "12"
                )
                .build()

            FirebaseAuth.getInstance()
                .sendSignInLinkToEmail(username, actionCodeSettings)
                .addOnCompleteListener { task ->
                    if(task.isSuccessful) {
                        Log.d(TAG, "Email sent")
                        _loginResult.value =
                            LoginResult(
                                userView = LoggedInUserView(displayName = "Check your email"),
                                state = LoginState.waitingForEmail)
                    }
                }
                .addOnFailureListener {
                    _loginResult.value = LoginResult(
                        error = R.string.login_failed,
                        state = LoginState.failed)
                }
        } catch (e: Throwable) {
            Result.Error(IOException("Error logging in", e))
        }
    }

    fun loginDataChanged(username: String) {
        if (!isEmailValid(username)) {
            _loginForm.value = LoginFormState(usernameError = R.string.invalid_username)
        } else {
            _loginForm.value = LoginFormState(isDataValid = true)
        }
    }

    fun login(email: String, originalEmail: String) {
        userRepository.login(email, originalEmail, _loginResult, _firebaseResult)
    }

    fun login(acct: GoogleSignInAccount) {
        userRepository.login(acct, _loginResult, _firebaseResult)
    }

    private fun isEmailValid(username: String): Boolean {
        return if (username.contains('@')) {
            Patterns.EMAIL_ADDRESS.matcher(username).matches()
        } else {
            username.isNotBlank()
        }
    }

    override fun clearListeners() {
        userRepository.cleanUpListeners()
    }
}
