package com.co.geek.rollingpaper.`interface`

import com.co.geek.rollingpaper.logic.BoxOption
import retrofit2.Call
import retrofit2.http.*

interface IGameFunctions {

    @Headers("Content-Type: application/json")
    @POST("game-takeAction?")
    fun takeAction(
        @Query("gameId") gameId: String,
        @Body boxOption: BoxOption
    ) : Call<String>
}