package com.co.geek.rollingpaper.ui.main

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.co.geek.rollingpaper.R
import com.co.geek.rollingpaper.logic.FirebaseState
import com.co.geek.rollingpaper.ui.ViewModelFactory
import com.co.geek.rollingpaper.ui.friends.Friends
import com.co.geek.rollingpaper.ui.game.BaseGame
import com.co.geek.rollingpaper.ui.login.LoginActivity
import com.co.geek.rollingpaper.ui.gameviewer.ResumeGame
import com.co.geek.rollingpaper.ui.startgroup.StartGroup
import com.co.geek.rollingpaper.ui.usernameselection.UsernameSelection
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.main_fragment.*

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var mainViewModel: MainViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onDestroy() {
        super.onDestroy()
        mainViewModel.clearListeners()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mainViewModel = ViewModelProvider(this, ViewModelFactory(context!!))
            .get(MainViewModel::class.java)

        updateUI(true)

        mainViewModel.getCurrentUser().observe(viewLifecycleOwner, Observer {user ->

            if(user != null) {

                if (user.alias.isEmpty()) {
                    Snackbar.make(main, "Alias needs to be set first", Snackbar.LENGTH_INDEFINITE)
                        .setAction("Move on") {
                            startActivity(Intent(context, UsernameSelection::class.java))
                        }.show()
                } else {
                    Toast.makeText(context, "Welcome back ${user.alias}", Toast.LENGTH_LONG).show()
                }
            }
        })

        mainViewModel.appState.observe(viewLifecycleOwner, Observer {
            val appState = it ?: return@Observer

            when(appState.state) {
                FirebaseState.Nothing -> TODO()
                FirebaseState.Succeeded -> updateUI(appState.enableUI)
                FirebaseState.Cancelled, FirebaseState.Failed -> {
                    updateUI(appState.enableUI)
                    Snackbar.make(main, appState.message!!, Snackbar.LENGTH_INDEFINITE)
                        .setAction("OK") {}.show()
                }
            }
        })

        bt_start_group.setOnClickListener{
            startActivity(Intent(context, StartGroup::class.java))
        }
        bt_sign_off.setOnClickListener{
            FirebaseAuth.getInstance().signOut()
            startActivity(Intent(context, LoginActivity::class.java))
        }
        bt_add_friend.setOnClickListener {
            startActivity(Intent(context, Friends::class.java))
        }
        bt_join.setOnClickListener {
            startActivity(Intent(context, ResumeGame::class.java))
        }
    }

    private fun updateUI(enabled: Boolean) {
        bt_start_group.isEnabled = enabled
        bt_sign_off.isEnabled = enabled
        bt_add_friend.isEnabled = enabled
        bt_join.isEnabled = enabled
    }


}
