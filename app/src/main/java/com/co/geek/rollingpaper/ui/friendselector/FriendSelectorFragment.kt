package com.co.geek.rollingpaper.ui.friendselector

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.co.geek.rollingpaper.R
import com.co.geek.rollingpaper.adapters.FriendSelectorAdapter
import com.co.geek.rollingpaper.ui.ViewModelFactory
import com.co.geek.rollingpaper.ui.startgroup.StartGroupFragment
import kotlinx.android.synthetic.main.friend_selector_fragment.*
import java.lang.Exception

class FriendSelectorFragment : Fragment() {

    private val TAG = FriendSelectorViewModel::class.java.name

    companion object {
        fun newInstance() = FriendSelectorFragment()
    }

    private lateinit var viewModel: FriendSelectorViewModel
    private lateinit var friendsAdapter: FriendSelectorAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.friend_selector_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this, ViewModelFactory(context!!)).get(FriendSelectorViewModel::class.java)

        //Initializing adapter
        rv_friend_list.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        friendsAdapter = FriendSelectorAdapter(arrayListOf())
        rv_friend_list.adapter = friendsAdapter

        viewModel.getFriends().observe(viewLifecycleOwner, Observer { friends ->
            try {
                friendsAdapter.updateList(friends)
            }
            catch (ex: Exception) {
                Log.e(TAG, "Something went wrong retrieving friends ${ex.message}")
            }
        })

        et_user_search.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                friendsAdapter.filterFriends(s.toString())
            }

        })
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.clearListeners()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.edit_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.action_done -> {
                val returnIntent = Intent()
                returnIntent.putExtra(StartGroupFragment.RI_FRIENDS, friendsAdapter.getSelected())
                activity!!.setResult(Activity.RESULT_OK, returnIntent)
                activity!!.finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
