package com.co.geek.rollingpaper.repository

import androidx.lifecycle.MutableLiveData
import com.co.geek.rollingpaper.logic.Campaign
import com.co.geek.rollingpaper.logic.login.FirebaseResult
import com.co.geek.rollingpaper.utils.FirebaseConsts
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase

class CampaignRepository : BaseRepository<Campaign>() {

    private var TAG = CampaignRepository::class.java.name

    override fun getAll(
        uid: String?,
        listener: MutableLiveData<FirebaseResult>?
    ): MutableLiveData<List<Campaign>> {
        if(list.value == null) {
            database.child(FirebaseConsts.CAMPAIGNS)
                .addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onCancelled(p0: DatabaseError) {
                        TODO("not implemented")
                    }

                    override fun onDataChange(p0: DataSnapshot) {
                        if(p0.exists()) {
                            val campaignArray = ArrayList<Campaign>()

                            p0.children.forEach {snap ->
                                campaignArray.add(Campaign(snap))
                            }
                            list.postValue(campaignArray.toList())
                        }
                    }
                })
        }

        return list
    }

    override fun add(item: Campaign, listener: MutableLiveData<FirebaseResult>?) {
        TODO("Not yet implemented")
    }

    override fun delete(item: Campaign, listener: MutableLiveData<FirebaseResult>?) {
        TODO("Not yet implemented")
    }

    override fun getSingle(
        uid: String?,
        listener: MutableLiveData<FirebaseResult>?
    ): MutableLiveData<Campaign> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}