package com.co.geek.rollingpaper.logic

enum class MapType {
    Undefined,
    Exploration,
    PvM
}