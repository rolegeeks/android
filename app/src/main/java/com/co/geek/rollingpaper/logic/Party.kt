package com.co.geek.rollingpaper.logic

import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.ktx.Firebase
import java.io.Serializable

class Party(): FirebaseItem(), Serializable {

    constructor(dataSnapshot: DataSnapshot) : this() {
        with(dataSnapshot) {
            if(exists()) {
                uid = key!!
                name = child("name").value.toString()
                child("campaign").let {campaignSnapshot ->
                    campaign = Campaign(campaignSnapshot)
                }
                val newMembers = arrayListOf<PartyUser>()
                child("members").children.forEach {user ->
                    val partyUser = user.getValue(PartyUser::class.java)
                    partyUser!!.uid = user.key!!
                    newMembers.add(partyUser)
                }
                members = newMembers
                started = child("started").value as Boolean
            }
        }
    }

    constructor(
        members: List<PartyUser>,
        name: String,
        campaign: Campaign,
        started: Boolean
    ) : this() {
        this.members = members
        this.name = name
        this.campaign = campaign
        this.started = started
    }

    var members: List<PartyUser> = listOf()
    var name: String = ""
    var campaign: Campaign = Campaign()
    var started = false


    override fun toFirebaseMap(): HashMap<String, Any> {
        return hashMapOf(
            "members" to listToMap(members),
            "name" to name,
            "campaign" to campaign,
            "started" to started
        )
    }
}