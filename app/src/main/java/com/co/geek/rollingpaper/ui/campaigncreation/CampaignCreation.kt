package com.co.geek.rollingpaper.ui.campaigncreation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.co.geek.rollingpaper.R
import com.co.geek.rollingpaper.ui.campaigncreation.CampaignCreationFragment

class CampaignCreation : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.campaign_creation_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, CampaignCreationFragment.newInstance())
                .commitNow()
        }
    }

}
