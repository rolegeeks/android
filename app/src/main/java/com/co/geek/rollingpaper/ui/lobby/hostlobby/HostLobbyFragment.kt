package com.co.geek.rollingpaper.ui.lobby.hostlobby

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.co.geek.rollingpaper.R
import com.co.geek.rollingpaper.adapters.ChatAdapter
import com.co.geek.rollingpaper.adapters.LobbyAdapter
import com.co.geek.rollingpaper.logic.FirebaseState
import com.co.geek.rollingpaper.ui.characterselection.CharacterSelection
import com.co.geek.rollingpaper.ui.game.BaseGame
import com.co.geek.rollingpaper.ui.game.GameHost
import com.co.geek.rollingpaper.ui.lobby.LobbyFragment
import com.co.geek.rollingpaper.ui.lobby.LobbyViewModel
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.host_lobby_fragment.*
import kotlinx.android.synthetic.main.lobby.*

class HostLobbyFragment : LobbyFragment() {

    private val TAG = HostLobbyFragment::class.java.name


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.lobby, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        //Initializing view model
        lobbyViewModel = ViewModelProvider(this).get(HostLobbyViewModel::class.java)

        super.onActivityCreated(savedInstanceState)

        //Setting startup behavior
        bt_lobby_action.isEnabled = false
        bt_lobby_action.text = getText(R.string.tv_start_group)

        lobbyViewModel.uiState.observe(viewLifecycleOwner, Observer {
            val uiState = it ?: return@Observer

            when(uiState.state) {
                FirebaseState.Nothing -> TODO()
                FirebaseState.Succeeded -> {
                    bt_lobby_action.isEnabled = it.enableUI
                }
                FirebaseState.Cancelled, FirebaseState.Failed -> {
                    bt_lobby_action.isEnabled = it.enableUI
                    Toast.makeText(context, it.message, Toast.LENGTH_LONG).show()
                }
                FirebaseState.Removed -> {
                }
            }
        })

        lobbyViewModel.deleteResult.observe(viewLifecycleOwner, Observer {
            val deleteResult = it ?: return@Observer

            when(deleteResult.state) {
                FirebaseState.Nothing -> TODO()
                FirebaseState.Succeeded -> {
                    activity?.onBackPressed()
                }
                FirebaseState.Failed -> {
                    Toast.makeText(context, it.message, Toast.LENGTH_LONG).show()
                }
                FirebaseState.Cancelled -> TODO()
            }
        })

        bt_lobby_action.setOnClickListener {
            lobbyViewModel.startGroup()
        }
    }

    override fun beginGame() {
        startActivity(Intent(context, GameHost::class.java)
            .putExtra(BaseGame.PARTY_ID, partyId))
        activity?.finish()
    }


    fun deleteParty() {
        lobbyViewModel.deleteParty()
    }

    companion object {
        @JvmStatic
        fun newInstance(partyId: String) =
            HostLobbyFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARTY_ID, partyId)
                }
            }
    }
}
