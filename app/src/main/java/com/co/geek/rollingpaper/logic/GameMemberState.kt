package com.co.geek.rollingpaper.logic

enum class GameMemberState {
    Undefined,
    Loading,
    Loaded
}