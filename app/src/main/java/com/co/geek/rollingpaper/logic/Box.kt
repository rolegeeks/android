package com.co.geek.rollingpaper.logic

import com.google.firebase.database.DataSnapshot
import java.io.Serializable

class Box(): Serializable {
    var mapType = MapType.Undefined
    var boxType = BoxType.Undefined
    var mapEnvironment = MapEnvironment.Undefined
    var options = arrayListOf<BoxOption>()
    var name = ""
    var narrative = ""
    var action : BoxAction? = null

    constructor(dataSnapshot: DataSnapshot) : this() {

        with(dataSnapshot) {

            if(exists()) {
                mapType = MapType.values()[child("mapType").value.toString().toInt()]
                boxType = BoxType.values()[child("boxType").value.toString().toInt()]
                mapEnvironment = MapEnvironment.values()[child("mapEnvironment").value.toString().toInt()]

                child("options").children.forEach { option ->
                    options.add(BoxOption(option))
                }

                name = child("name").value.toString()
                narrative = child("narrative").value.toString()
            }
        }
    }
}