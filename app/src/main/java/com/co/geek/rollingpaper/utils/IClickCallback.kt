package com.co.geek.rollingpaper.utils

interface IClickCallback<T> {

    fun onClick(item: T)
}