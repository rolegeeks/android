package com.co.geek.rollingpaper.ui.campaigncreation

import androidx.lifecycle.LiveData
import com.co.geek.rollingpaper.logic.Campaign
import com.co.geek.rollingpaper.repository.CampaignRepository
import com.co.geek.rollingpaper.ui.BaseViewModel

class CampaignCreationViewModel : BaseViewModel() {

    private var campaignRepository: CampaignRepository = CampaignRepository()

    fun getCampaigns() : LiveData<List<Campaign>> {
        return campaignRepository.getAll()
    }

    override fun clearListeners() {
        campaignRepository.cleanUpListeners()
    }
}
