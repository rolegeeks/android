package com.co.geek.rollingpaper.adapters

import android.provider.Telephony
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Transformation
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.Transformations
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.co.geek.rollingpaper.R
import com.co.geek.rollingpaper.logic.Friend
import com.co.geek.rollingpaper.logic.PartyUser
import com.co.geek.rollingpaper.logic.User
import com.co.geek.rollingpaper.utils.PhotoRowDiffCallback
import kotlinx.android.synthetic.main.selected_friend_model_adapter.view.*
import java.lang.Exception
import java.util.*
import kotlin.collections.ArrayList

class FriendSelectorAdapter(private var _friendsList: ArrayList<Friend>)
    : RecyclerView.Adapter<FriendSelectorAdapter.ViewHolder>() {

    private var _filtering = false
    private var _firstUse = true
    private var _filteredFriends = ArrayList<Friend>()
    private val _onClickListener: View.OnClickListener

    init {
        _onClickListener = View.OnClickListener { v ->
            try {
                val item = v.tag as Friend
                item.selected = !item.selected
                notifyDataSetChanged()
            }
            catch (ex: Exception) {
                TODO("Log this crap")
            }

        }
    }

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val friendNickname: TextView = itemView.tv_friend_nickname
        val selectedImage: ImageView = itemView.iv_check
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater
            .from(parent.context)
            .inflate(R.layout.selected_friend_model_adapter, parent, false))
    }

    override fun getItemCount(): Int {
        return getCurrentList().size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val list = getCurrentList()

        with(holder) {
            friendNickname.text = list[position].alias
            itemView.tag = list[position]
            itemView.setOnClickListener(_onClickListener)

            if(list[position].selected) {
                selectedImage.visibility = View.VISIBLE
            }
            else {
                selectedImage.visibility = View.GONE
            }
        }
    }

    fun updateList(newList: List<Friend>) {
        //Updating...
        _friendsList = ArrayList(newList)
        notifyDataSetChanged()
    }

    fun getSelected() : ArrayList<PartyUser> {
        val output = arrayListOf<PartyUser>()
        _friendsList.forEach {
            if(it.selected) {
                val partyUser = PartyUser()
                partyUser.alias = it.alias
                partyUser.token = it.token
                partyUser.uid = it.uid
                output.add(partyUser)
            }
        }

        return output
    }

    fun filterFriends(query: String) {
        _filtering= true

        val newFriends = _friendsList.filter { friend ->
            friend.alias.toLowerCase(Locale.ROOT).contains(query.toLowerCase(Locale.ROOT))
        } as ArrayList<Friend>

        if(newFriends.size == _friendsList.size && !_firstUse) {
            DiffUtil.calculateDiff(PhotoRowDiffCallback(newFriends, _filteredFriends), false).dispatchUpdatesTo(this)
        }
        else {
            DiffUtil.calculateDiff(PhotoRowDiffCallback(newFriends, _friendsList), false).dispatchUpdatesTo(this)
            _firstUse = false
        }
        _filteredFriends = newFriends
    }

    private fun getCurrentList() : ArrayList<Friend> {
        return if (_filtering) {
            _filteredFriends
        } else {
            _friendsList
        }
    }
}