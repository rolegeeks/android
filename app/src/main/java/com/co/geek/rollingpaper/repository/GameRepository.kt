package com.co.geek.rollingpaper.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.co.geek.rollingpaper.`interface`.IGameFunctions
import com.co.geek.rollingpaper.logic.*
import com.co.geek.rollingpaper.logic.login.FirebaseResult
import com.co.geek.rollingpaper.utils.FirebaseConsts
import com.co.geek.rollingpaper.utils.FirebaseConsts.Companion.GAMES
import com.co.geek.rollingpaper.utils.FirebaseConsts.Companion.GAME_CHECKEDINMEMBERS
import com.co.geek.rollingpaper.utils.FirebaseConsts.Companion.GAME_PROGRESS
import com.co.geek.rollingpaper.utils.RetrofitClient
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.ktx.Firebase
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class GameRepository(val gameStatus: MutableLiveData<GameStatus>?, val gameId: String): BaseRepository<Game>() {

    private val TAG = GameRepository::class.java.name
    private var currentBoxId = 0
    private var gameFunctionsAPI: IGameFunctions = RetrofitClient.instance.create(
        IGameFunctions::class.java)

    override fun getAll(
        uid: String?,
        listener: MutableLiveData<FirebaseResult>?
    ): MutableLiveData<List<Game>> {
        return MutableLiveData()
    }

    override fun getSingle(
        uid: String?,
        listener: MutableLiveData<FirebaseResult>?
    ): MutableLiveData<Game> {
        if(uid != null) {

            //I just need to get the game once, nothing else will change
            val singleListener = database.child(GAMES).child(uid)
                .addValueEventListener(object : ValueEventListener {
                    override fun onCancelled(p0: DatabaseError) {
                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                    }

                    override fun onDataChange(p0: DataSnapshot) {
                        if(p0.value == null) {
                            single.value = null
                        } else {
                            val updatedGame = Game(p0)

                            single.value = updatedGame

                            val box = updatedGame.boxes[currentBoxId]

                            when {
                                updatedGame.progress.boxId != currentBoxId -> {
                                    currentBoxId = updatedGame.progress.boxId
                                    gameStatus?.value = GameStatus(
                                        progressState = ProgressStatus.Advance,
                                        currentBox = box
                                    )
                                }
                                updatedGame.progress.checkedInMembers.count() != updatedGame.members.count() -> {
                                    gameStatus?.value = GameStatus(
                                        progressState = ProgressStatus.WaitingForPlayers,
                                        currentBox = box,
                                        message = "Waiting for members to sync"
                                    )
                                }
                                updatedGame.progress.checkedInMembers.count() == updatedGame.members.count()
                                        && updatedGame.progress.boxId == 0-> {
                                    //We can safely delete the party object
                                    database.child(FirebaseConsts.PARTY).child(uid).removeValue()
                                    boxCheckIn()
                                }
                                //Ok so this means we need to update the game process... Lets see how
                                updatedGame.progress.boxId == currentBoxId -> {

                                    //If this is null, don't do anything, nothing significant happened
                                    if(box.action != null ) {
                                        gameStatus?.value = GameStatus(
                                            progressState = ProgressStatus.ActionTaken,
                                            currentBox = box
                                        )
                                    } else {
                                        Log.i(TAG, "Getting updates from game, but no action")
                                    }
                                }
                                else -> {
                                    gameStatus?.value = GameStatus(
                                        progressState = ProgressStatus.Synced
                                    )
                                }
                            }
                        }
                    }
                })
            trackListener(singleListener)
        }

        return single
    }

    override fun add(item: Game, listener: MutableLiveData<FirebaseResult>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun delete(item: Game, listener: MutableLiveData<FirebaseResult>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    //Consider moving to its own repo
    fun getPendingGames() : LiveData<List<PendingGame>> {
        val pendingGame = MutableLiveData<List<PendingGame>>()

        val id = Firebase.auth.uid


        if(id != null) {
            val partiesListener = database
                .child(FirebaseConsts.USERS)
                .child(id)
                .child(FirebaseConsts.USER_PENDING_GAMES)
                .addValueEventListener(object : ValueEventListener {

                    override fun onCancelled(p0: DatabaseError) {
                        Log.e(TAG, "Failed to get user parties")
                        pendingGame.value = listOf()
                    }

                    override fun onDataChange(p0: DataSnapshot) {
                        val list = arrayListOf<PendingGame>()
                        p0.children.forEach { data ->
                            list.add(PendingGame(data.value.toString()))
                        }

                        //First get the parties and then get the games
                        database
                            .child(FirebaseConsts.PARTY)
                            .addListenerForSingleValueEvent(object : ValueEventListener {
                                override fun onCancelled(p0: DatabaseError) {
                                    Log.e(TAG, "Failed to get all parties")
                                    pendingGame.value = listOf()
                                }

                                override fun onDataChange(p0: DataSnapshot) {
                                    try {
                                        p0.children.forEach { createdParty ->

                                            list.first { p -> p.uid == createdParty.key!! }.let {
                                                it.isRunning = false
                                                it.fillGameFromParty(createdParty)
                                            }
                                        }
                                    } catch (ex: Exception) {
                                        Log.e(TAG,"Something went wrong. ${ex.message}")
                                        pendingGame.value = null
                                    }

                                    database
                                        .child(GAMES)
                                        .addListenerForSingleValueEvent(object : ValueEventListener {
                                            override fun onCancelled(p0: DatabaseError) {
                                                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                                            }

                                            override fun onDataChange(p0: DataSnapshot) {
                                                try {
                                                    p0.children.forEach { game ->
                                                        list.first{ p -> p.uid == game.key!! }.let {
                                                            it.isRunning = true
                                                            it.fillGame(game)
                                                        }
                                                    }
                                                    pendingGame.value = list
                                                } catch (ex: Exception) {
                                                    Log.e(TAG,"Something went wrong. ${ex.message}")
                                                    pendingGame.value = null
                                                }
                                            }
                                        })
                                }

                            })
                    }

                })
            trackListener(partiesListener)
        }

        return pendingGame
    }

    fun boxCheckIn() {
        database
            .child(GAMES)
            .child(gameId)
            .child(GAME_PROGRESS)
            .child(GAME_CHECKEDINMEMBERS)
            .child(Firebase.auth.uid!!)
            .setValue(Firebase.auth.uid)
    }

    fun listenToProgress(uid: String) : LiveData<GameStatus> {
        val gameStatus : MutableLiveData<GameStatus> = MutableLiveData()
        //Here we will listen for the progress flag
        val listener = database.child(GAMES).child(uid).child(GAME_PROGRESS)
            .addValueEventListener(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {
                    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }

                override fun onDataChange(p0: DataSnapshot) {
                    Log.d(TAG, p0.key)
                }
            })

        trackListener(listener)

        return gameStatus
    }

    fun setActionTaken(boxOption: BoxOption) {
        gameFunctionsAPI.takeAction(gameId, boxOption).enqueue(object : Callback<String> {

            override fun onFailure(call: Call<String>, t: Throwable) {
                Log.e(TAG, t.message)
            }

            override fun onResponse(call: Call<String>, response: Response<String>) {
                Log.e(TAG, response.message())
            }

        })
    }
}