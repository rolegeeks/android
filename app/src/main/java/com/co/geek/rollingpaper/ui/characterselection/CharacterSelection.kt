package com.co.geek.rollingpaper.ui.characterselection

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.co.geek.rollingpaper.R
import com.co.geek.rollingpaper.logic.Character
import com.co.geek.rollingpaper.ui.lobby.LobbyViewModel.Companion.EX_PARTY_ID

class CharacterSelection : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.character_selection)

        if(savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, CharacterListFragment.
                    newInstance())
                .commit()
        } else {
            //Check on which fragment we were and just place it again
        }
    }

    override fun onBackPressed() {
        if(supportFragmentManager.backStackEntryCount > 0) {
            supportFragmentManager.popBackStack()
        } else {
            super.onBackPressed()
        }
    }
}
