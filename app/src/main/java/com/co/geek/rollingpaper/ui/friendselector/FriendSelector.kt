package com.co.geek.rollingpaper.ui.friendselector

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.co.geek.rollingpaper.R
import com.co.geek.rollingpaper.ui.friendselector.FriendSelectorFragment

class FriendSelector : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.friend_selector_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, FriendSelectorFragment.newInstance())
                .commitNow()
        }
    }
}
