package com.co.geek.rollingpaper.ui

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.co.geek.rollingpaper.ui.friends.FriendsViewModel
import com.co.geek.rollingpaper.ui.friendselector.FriendSelectorViewModel
import com.co.geek.rollingpaper.ui.game.GameViewModel
import com.co.geek.rollingpaper.ui.login.LoginViewModel
import com.co.geek.rollingpaper.ui.main.MainViewModel
import com.co.geek.rollingpaper.ui.startgroup.StartGroupViewModel
import com.co.geek.rollingpaper.ui.usernameselection.UsernameSelectionViewModel
import java.lang.IllegalArgumentException

@Suppress("UNCHECKED_CAST")
class ViewModelFactory(private val context: Context, private val uid: String? = null) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {

        return when {
            modelClass.isAssignableFrom(MainViewModel::class.java) -> {
                MainViewModel(context) as T
            }
            modelClass.isAssignableFrom(LoginViewModel::class.java) -> {
                LoginViewModel(context) as T
            }
            modelClass.isAssignableFrom(StartGroupViewModel::class.java) -> {
                StartGroupViewModel(context) as T
            }
            modelClass.isAssignableFrom(FriendsViewModel::class.java) -> {
                FriendsViewModel(context) as T
            }
            modelClass.isAssignableFrom(FriendSelectorViewModel::class.java) -> {
                FriendSelectorViewModel(context) as T
            }
            modelClass.isAssignableFrom(UsernameSelectionViewModel::class.java) -> {
                UsernameSelectionViewModel(context) as T
            }
            modelClass.isAssignableFrom(GameViewModel::class.java) -> {
                GameViewModel(context, uid!!) as T
            }
            else -> {
                throw IllegalArgumentException("Unknown view model class")
            }
        }
    }
}