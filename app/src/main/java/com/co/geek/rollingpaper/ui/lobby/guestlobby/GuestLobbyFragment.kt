package com.co.geek.rollingpaper.ui.lobby.guestlobby

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.co.geek.rollingpaper.R
import com.co.geek.rollingpaper.adapters.ChatAdapter
import com.co.geek.rollingpaper.adapters.LobbyAdapter
import com.co.geek.rollingpaper.logic.FirebaseState
import com.co.geek.rollingpaper.logic.PartyState
import com.co.geek.rollingpaper.ui.game.BaseGame
import com.co.geek.rollingpaper.ui.game.GameGuest
import com.co.geek.rollingpaper.ui.lobby.LobbyFragment
import com.co.geek.rollingpaper.ui.main.MainActivity
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.guest_lobby_fragment.*
import kotlinx.android.synthetic.main.lobby.*
import java.lang.Exception

private const val ARG_PARTY_ID = "param1"

class GuestLobbyFragment : LobbyFragment() {

    private val TAG = GuestLobbyFragment::class.java.name

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.lobby, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        lobbyViewModel = ViewModelProvider(this).get(GuestLobbyViewModel::class.java)

        super.onActivityCreated(savedInstanceState)

        bt_lobby_action.text = getText(R.string.tv_ready)

        lobbyViewModel.uiState.observe(viewLifecycleOwner, Observer {
            val uiState = it ?: return@Observer

            when(uiState.state) {
                FirebaseState.Nothing -> TODO()
                FirebaseState.Succeeded -> {
                    bt_lobby_action.isEnabled = it.enableUI
                }
                FirebaseState.Cancelled, FirebaseState.Failed -> {
                    bt_lobby_action.isEnabled = it.enableUI
                    Toast.makeText(context, it.message, Toast.LENGTH_LONG).show()
                }
                FirebaseState.Removed -> {
                    Snackbar.make(lobby_container, uiState.message!!, Snackbar.LENGTH_INDEFINITE)
                        .setAction("OK") {
                            //Horrible hack to avoid prompting but we preserve navigation stack
                            backPressed = true
                            activity?.onBackPressed()
                        }
                        .show()
                }
            }
        })

        lobbyViewModel.updateStatus.observe(viewLifecycleOwner, Observer {
            try {
                val updateState = it ?: return@Observer

                when(updateState.state) {
                    FirebaseState.Nothing -> TODO()
                    FirebaseState.Succeeded -> {
                        if(joinedAdapter.updateStatus()) {
                            bt_lobby_action.text = "Not ready"
                        }
                        else {
                            bt_lobby_action.text = "Ready"
                        }
                    }
                    FirebaseState.Failed, FirebaseState.Cancelled -> {
                        Log.e(TAG, "Something failed, ${it.message}")
                        Toast.makeText(
                            context,
                            "Could not change state to ready",
                            Toast.LENGTH_LONG).show()
                    }
                }
            } catch (ex: Exception) {
                Log.e(TAG, "Something broke while changing state ${ex.message}")
            }
        })

        lobbyViewModel.joinedStatus.observe(viewLifecycleOwner, Observer {
            val joinedStatus = it ?: return@Observer

            when(joinedStatus.state) {
                PartyState.Joined -> {
                    Toast.makeText(context, "Joined successfully", Toast.LENGTH_LONG).show()
                }
                PartyState.Left -> {
                    activity?.onBackPressed()
                }
                PartyState.Error -> {
                    Toast.makeText(context, joinedStatus.message, Toast.LENGTH_LONG).show()
                }
            }
        })

        bt_lobby_action.setOnClickListener {
            lobbyViewModel.updateStatus(partyId)
        }

        if(partyId.isNotEmpty()) {
            //Always true here because this is the onCreate method
            lobbyViewModel.updateJoinedStatus(partyId, true)
        }

    }

    override fun beginGame() {
        startActivity(Intent(context, GameGuest::class.java)
            .putExtra(BaseGame.PARTY_ID, partyId))
        activity?.finish()
    }

    fun leaveParty() {
        lobbyViewModel.updateJoinedStatus(partyId, false)
    }

    companion object {
        @JvmStatic
        fun newInstance(partyId: String) =
            GuestLobbyFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARTY_ID, partyId)
                }
            }
    }
}
