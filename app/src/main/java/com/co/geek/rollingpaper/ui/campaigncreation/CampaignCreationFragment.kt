package com.co.geek.rollingpaper.ui.campaigncreation

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.co.geek.rollingpaper.R
import com.co.geek.rollingpaper.adapters.CampaignAdapter
import kotlinx.android.synthetic.main.campaign_creation_fragment.*

class CampaignCreationFragment : Fragment() {

    companion object {
        fun newInstance() = CampaignCreationFragment()
    }

    private lateinit var viewModel: CampaignCreationViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        return inflater.inflate(R.layout.campaign_creation_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        //Initializing recycler view
        rv_campaigns.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)

        viewModel = ViewModelProvider(this).get(CampaignCreationViewModel::class.java)

        viewModel.getCampaigns().observe(viewLifecycleOwner, Observer {

            val campaignAdapter = CampaignAdapter(it!!)
            rv_campaigns.adapter = campaignAdapter
        })
    }

}
