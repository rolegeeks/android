package com.co.geek.rollingpaper.logic

enum class MapEnvironment {
    Undefined,
    Indoor,
    Outdoor
}