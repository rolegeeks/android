package com.co.geek.rollingpaper.logic

enum class FirebaseState {
    Nothing,
    Succeeded,
    Cancelled,
    Failed,
    Removed
}