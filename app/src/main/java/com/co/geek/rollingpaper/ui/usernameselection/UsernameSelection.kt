package com.co.geek.rollingpaper.ui.usernameselection

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.co.geek.rollingpaper.R
import com.co.geek.rollingpaper.ui.login.afterTextChanged
import com.co.geek.rollingpaper.ui.main.MainActivity
import kotlinx.android.synthetic.main.activity_username_selection.*

class UsernameSelection : AppCompatActivity() {

    private val TAG = UsernameSelection::class.java.name
    private lateinit var usersViewModel: UsernameSelectionViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_username_selection)

        usersViewModel = ViewModelProvider(this).get(UsernameSelectionViewModel::class.java)

        usersViewModel.aliasFormState.observe(this@UsernameSelection, Observer {
            val aliasState = it ?: return@Observer

            bt_set.isEnabled = it.canWork

            when(aliasState.state) {
                AliasState.Valid -> {}
                AliasState.Invalid -> et_alias.error = it.message
                AliasState.Successful -> startActivity(Intent(this, MainActivity::class.java))
                AliasState.Loaded -> Log.i(TAG, "Aliases loaded, unlocking UI")
                AliasState.Failed -> {
                    showToast(it.message!!)
                    et_alias.isEnabled = false
                }
                null -> TODO()
            }
        })

        et_alias.afterTextChanged {
            usersViewModel.aliasChanged(et_alias.text.toString())
        }
        //Getting all aliases
        usersViewModel.getAliases()

        bt_set.setOnClickListener {
            usersViewModel.setAlias(et_alias.text.toString())
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        usersViewModel.clearListeners()
    }

    private fun showToast(message: String) {
        Toast.makeText(
            applicationContext,
            message,
            Toast.LENGTH_LONG
        ).show()
    }
}
