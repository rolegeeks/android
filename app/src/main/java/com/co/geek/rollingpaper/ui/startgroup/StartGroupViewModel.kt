package com.co.geek.rollingpaper.ui.startgroup

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.co.geek.rollingpaper.logic.Campaign
import com.co.geek.rollingpaper.logic.Party
import com.co.geek.rollingpaper.logic.PartyUser
import com.co.geek.rollingpaper.logic.User
import com.co.geek.rollingpaper.logic.login.FirebaseResult
import com.co.geek.rollingpaper.repository.CampaignRepository
import com.co.geek.rollingpaper.repository.PartyRepository
import com.co.geek.rollingpaper.repository.UserRepository
import com.co.geek.rollingpaper.ui.BaseViewModel
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

class StartGroupViewModel(context: Context) : BaseViewModel() {

    private val TAG = StartGroupViewModel::class.java.name

    private var campaignRepository: CampaignRepository = CampaignRepository()
    private var partyRepository: PartyRepository = PartyRepository()
    private var userRepository: UserRepository = UserRepository(context)

    private var user : MutableLiveData<User> = MutableLiveData()

    private val _firebaseResult = MutableLiveData<FirebaseResult>()
    val firebaseResult: LiveData<FirebaseResult> = _firebaseResult

    fun getCampaigns() : LiveData<List<Campaign>> {
        return campaignRepository.getAll()
    }

    fun createParty(members: ArrayList<PartyUser>, partyName: String, campaign: Campaign) {
        //we need to add ourselves to this weird ass party
        if(!members.any { member -> member.host })
        {
            val hostUser = PartyUser()
            with(user.value!!) {
                hostUser.host = true
                hostUser.alias = alias
                hostUser.inParty = true
                hostUser.uid = Firebase.auth.uid!!
                hostUser.token = token
            }
            members.add(hostUser)
        }

        return partyRepository.add(Party(members, partyName, campaign, false), _firebaseResult)
    }

    fun retrieveUser() : LiveData<User> {
        user = userRepository.getSingle(Firebase.auth.uid)
        return user
    }

    override fun clearListeners() {
        campaignRepository.cleanUpListeners()
        partyRepository.cleanUpListeners()
        userRepository.cleanUpListeners()
    }
}
