package com.co.geek.rollingpaper.ui.gameviewer

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.co.geek.rollingpaper.R
import com.co.geek.rollingpaper.adapters.GameSectionAdapter
import com.co.geek.rollingpaper.adapters.PendingGameAdapter
import kotlinx.android.synthetic.main.activity_resume_game.*

class ResumeGame : AppCompatActivity() {

    private val TAG = ResumeGame::class.java.name

    private lateinit var gameViewerViewModel: GameViewerViewModel
    private lateinit var pendingGamesAdapter: GameSectionAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_resume_game)

        gameViewerViewModel = ViewModelProvider(this).get(GameViewerViewModel::class.java)

        rv_games.layoutManager = LinearLayoutManager(applicationContext, RecyclerView.VERTICAL, false)
        pendingGamesAdapter = GameSectionAdapter(hashMapOf())
        rv_games.adapter = pendingGamesAdapter

        gameViewerViewModel.getPendingParties().observe(this, Observer {
            val games = it ?: return@Observer

            pendingGamesAdapter.formatGames(games)
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        gameViewerViewModel.clearListeners()
    }
}
