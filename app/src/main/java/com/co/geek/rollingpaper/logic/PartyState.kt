package com.co.geek.rollingpaper.logic

enum class PartyState {
    Joined,
    Left,
    Error
}