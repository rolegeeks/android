package com.co.geek.rollingpaper.ui.usernameselection

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.co.geek.rollingpaper.repository.UserRepository
import com.co.geek.rollingpaper.ui.BaseViewModel

class UsernameSelectionViewModel(context: Context) : BaseViewModel() {

    private var errorMessage: String? = null
    private var userRepository: UserRepository = UserRepository(context)
    private val _aliasForm: MutableLiveData<AliasFormState> = MutableLiveData()
    val aliasFormState: LiveData<AliasFormState> = _aliasForm

    fun getAliases() {
        userRepository.getAliases(_aliasForm)
    }

    fun aliasChanged(text: String) {
        if(isAliasValid(text)) {
            _aliasForm.value = AliasFormState(
                state = AliasState.Valid,
                canWork = true
            )
        } else {
            _aliasForm.value = AliasFormState(
                state = AliasState.Invalid,
                message = errorMessage
            )
        }
    }

    private fun isAliasValid(alias: String) : Boolean {
        return if (alias.length in 5..20) {
            //before checking the other aliases, first lets check if the string is right
            //we need to verify it doesn't have any non allowed character
            if(userRepository.aliases.contains(alias))
            {
                errorMessage = "Alias already exists"
                false
            }
            else
            {
                true
            }

        } else {
            errorMessage = "Length must be between 4 and 20 characters"
            false
        }
    }

    fun setAlias(alias: String) {
        userRepository.setAlias(_aliasForm, alias)
    }

    override fun clearListeners() {
        userRepository.cleanUpListeners()
    }
}