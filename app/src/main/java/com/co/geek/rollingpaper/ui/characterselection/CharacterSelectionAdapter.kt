package com.co.geek.rollingpaper.ui.characterselection

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.co.geek.rollingpaper.R
import com.co.geek.rollingpaper.logic.Character
import kotlinx.android.synthetic.main.character_list_content.view.*
import java.lang.Exception

class CharacterSelectionAdapter(private var values: List<Character>)
    : RecyclerView.Adapter<CharacterSelectionAdapter.ViewHolder>() {

    private val TAG = CharacterSelectionAdapter::class.java.name

    private val onClickListener: View.OnClickListener

    init {
        onClickListener = View.OnClickListener { v ->
            val item = v.tag as Character
            val activity = v.context as CharacterSelection

            try{
                activity.supportFragmentManager.beginTransaction()
                    .replace(R.id.container, CharacterDetailFragment.newInstance(item))
                    .addToBackStack(null)
                    .commit()
            } catch (ex: Exception) {
                Log.e(TAG, ex.message)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.character_list_content, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]

        with(holder) {
            holder.characterName.text = item.alias

            //TODO("Set image with glide")
//            holder.characterImage. = item.content
            itemView.tag = item
            itemView.setOnClickListener(onClickListener)
        }
    }

    override fun getItemCount() = values.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val characterName: TextView = view.tv_character_name
        val characterImage: ImageView = view.iv_character_logo
    }

    fun updateCharacterList(list: List<Character>) {
        values = ArrayList(list)
        notifyDataSetChanged()
    }
}