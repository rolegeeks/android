package com.co.geek.rollingpaper.ui.main

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.co.geek.rollingpaper.logic.User
import com.co.geek.rollingpaper.logic.login.FirebaseResult
import com.co.geek.rollingpaper.repository.UserRepository
import com.co.geek.rollingpaper.ui.BaseViewModel
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

class MainViewModel(context: Context) : BaseViewModel() {

    private var userRepository: UserRepository = UserRepository(context)
    private var _appState: MutableLiveData<FirebaseResult> = MutableLiveData()
    var appState: LiveData<FirebaseResult> = _appState

    fun getCurrentUser() : LiveData<User> {
        return userRepository.getSingle(Firebase.auth.uid, _appState)
    }

    override fun clearListeners() {
        userRepository.cleanUpListeners()
    }

}
