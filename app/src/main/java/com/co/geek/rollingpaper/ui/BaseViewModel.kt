package com.co.geek.rollingpaper.ui

import androidx.lifecycle.ViewModel

abstract class BaseViewModel: ViewModel() {
    abstract fun clearListeners()
}