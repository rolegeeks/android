package com.co.geek.rollingpaper.logic

import com.google.firebase.database.DataSnapshot
import java.io.Serializable

class BoxOption(): FirebaseItem(), Serializable {

    var narrative = ""
    var boxOptionType = BoxOptionType.Undefined
    var dice = Dice()
    var result = ""

    constructor(dataSnapshot: DataSnapshot) : this() {
        with(dataSnapshot) {
            if(exists()) {
                narrative = child("narrative").value.toString()
                boxOptionType = BoxOptionType.values()[child("boxOptionType").value.toString().toInt()]
                result = child("result").value.toString()
                uid = key.toString()
            }
        }
    }

    override fun toFirebaseMap(): HashMap<String, Any> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}