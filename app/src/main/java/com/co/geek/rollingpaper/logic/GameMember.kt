package com.co.geek.rollingpaper.logic

import com.google.firebase.database.DataSnapshot
import java.io.Serializable

class GameMember() : FirebaseItem(), Serializable {

    var alias = ""
    var character = Character()
    var host = false
    var currentBox = 0
    var state = GameMemberState.Undefined

    constructor(dataSnapshot: DataSnapshot) : this() {
        with(dataSnapshot) {
            if(exists()) {
                alias = child("alias").value.toString()
                character = Character(child("character"))
                host = child("host").value as Boolean
                uid = key!!
            }
        }
    }

    override fun toFirebaseMap(): HashMap<String, Any> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}