package com.co.geek.rollingpaper.ui.lobby.guestlobby

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import com.co.geek.rollingpaper.R
import com.co.geek.rollingpaper.ui.lobby.LobbyViewModel.Companion.EX_PARTY_ID
import com.co.geek.rollingpaper.utils.ConfirmationDialogFragment

class GuestLobby : AppCompatActivity(), ConfirmationDialogFragment.ConfirmationDialogListener {

    private lateinit var guestLobbyFragment: GuestLobbyFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.group_waiting_activity)

        if (savedInstanceState == null) {
            guestLobbyFragment = GuestLobbyFragment
                .newInstance(intent.extras[EX_PARTY_ID] as String)

            supportFragmentManager.beginTransaction()
                .replace(R.id.container, guestLobbyFragment)
                .commitNow()
        }
    }

    override fun onBackPressed() {

        if(guestLobbyFragment.backPressed) {
            guestLobbyFragment.backPressed = false
            super.onBackPressed()
        } else {
            val dialog = ConfirmationDialogFragment("Are you sure you want to leave the party?");
            dialog.show(supportFragmentManager, "ConfirmationDialogFragment")
        }
    }

    override fun onDialogPositiveClick(dialog: DialogFragment) {
        //we need to ask the fragment to delete the party
        val fragment = supportFragmentManager.fragments[0] as GuestLobbyFragment
        fragment.leaveParty()
        guestLobbyFragment.backPressed = true
    }

    override fun onDialogNegativeClick(dialog: DialogFragment) {
        guestLobbyFragment.backPressed = false
    }

    fun goBack() {
        guestLobbyFragment.backPressed = true
        onBackPressed()
    }
}
