package com.co.geek.rollingpaper.logic

import com.google.firebase.database.DataSnapshot
import java.io.Serializable

class Character() : FirebaseItem(), Serializable {

    var alias = ""
    var characterClass: String = ""
    var stats: CharacterStats = CharacterStats()

    constructor(snapshot: DataSnapshot) : this() {
        if(snapshot.exists()) {
            this.uid = snapshot.key!!
            this.characterClass = snapshot.child("characterClass").value as String
            this.alias = snapshot.child("alias").value as String
            this.stats = CharacterStats(snapshot.child("stats"))

        } else {
            throw IllegalArgumentException("Empty snapshot")
        }
    }

    override fun toFirebaseMap(): HashMap<String, Any> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}