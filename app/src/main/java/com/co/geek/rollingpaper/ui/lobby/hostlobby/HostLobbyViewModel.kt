package com.co.geek.rollingpaper.ui.lobby.hostlobby

import com.co.geek.rollingpaper.logic.FirebaseState
import com.co.geek.rollingpaper.logic.login.FirebaseResult
import com.co.geek.rollingpaper.ui.lobby.LobbyViewModel

class HostLobbyViewModel : LobbyViewModel() {

    override fun updateStatus(partyId: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun updateJoinedStatus(partyId: String, joined: Boolean) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun deleteParty() {
        partyRepository.delete(partyRepository.getUnit(), _deleteResult)
    }

    override fun startGroup() {
        val party = partyRepository.getUnit()
        val characters = characterRepository.getList()

        val selectedCharacters = characters.filter {
            party.members.any{ member -> member.characterId == it.uid }
        }

        if(!selectedCharacters.isNullOrEmpty()) {
            party.campaign.characterList = ArrayList(selectedCharacters)
            //We need to obtain a list of character objects and set it to the party
            partyRepository.startGroup(party, _uiState)
        } else {
            _uiState.value = FirebaseResult(
                state = FirebaseState.Failed,
                message = "Error matching characters with members, please restart the party"
            )
        }
    }
}
