package com.co.geek.rollingpaper.ui.characterselection

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.co.geek.rollingpaper.R
import kotlinx.android.synthetic.main.character_list.*


private const val ARG_PARTY_ID = "param1"

class CharacterListFragment : Fragment() {

    private val TAG = CharacterListFragment::class.java.name

    private lateinit var characterListViewModel: CharacterListViewModel
    private lateinit var characterSelectionAdapter: CharacterSelectionAdapter

    private var partyId: String = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.character_list, container, false)
    }

    override fun onDestroy() {
        super.onDestroy()
        characterListViewModel.clearListeners()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        characterListViewModel = ViewModelProvider(this).get(CharacterListViewModel::class.java)

        character_list.layoutManager = GridLayoutManager(
            context,
            2,
            GridLayoutManager.VERTICAL,
            false)
        characterSelectionAdapter = CharacterSelectionAdapter(listOf())
        character_list.adapter = characterSelectionAdapter

        characterListViewModel.getUserCharacters().observe(viewLifecycleOwner, Observer {
            val characters = it ?: return@Observer

            characterSelectionAdapter.updateCharacterList(characters)
        })
    }

    companion object {
        @JvmStatic
        fun newInstance() = CharacterListFragment()
    }
}
