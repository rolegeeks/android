package com.co.geek.rollingpaper.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.co.geek.rollingpaper.R
import com.co.geek.rollingpaper.logic.PartyUser
import com.co.geek.rollingpaper.utils.SwipeToDeleteCallback
import kotlinx.android.synthetic.main.party_friends_adapter.view.*

class PartyUserAdapter(private val context: Context, private var _list: ArrayList<PartyUser>)
    : RecyclerView.Adapter<PartyUserAdapter.ViewHolder>() {

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val friendNickName: TextView = itemView.tv_friend_nickname
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)

        val friendsSwipeHandler = object : SwipeToDeleteCallback(context) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                removeItem(viewHolder.adapterPosition)
            }
        }
        val friendTouchHelper = ItemTouchHelper(friendsSwipeHandler)


        friendTouchHelper.attachToRecyclerView(recyclerView)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.party_friends_adapter, parent, false))
    }

    override fun getItemCount(): Int {
        return _list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(holder) {
            friendNickName.text = _list[position].alias
        }
    }

    fun removeItem(position: Int) {
        _list.removeAt(position)
        notifyDataSetChanged()
    }


    fun updateList(newList: List<PartyUser>) {
        //Updating...
        _list = ArrayList(newList)
        notifyDataSetChanged()
    }

    fun getList() : ArrayList<PartyUser> {
        return _list
    }
}