package com.co.geek.rollingpaper.logic

//TODO("What else?")
enum class BoxOptionType {
    Undefined,
    MoveForward,
    MoveBackwards,
    MoveLeft,
    MoveRight,
    Runaway,
    Fight
}