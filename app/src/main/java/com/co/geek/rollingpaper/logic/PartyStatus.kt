package com.co.geek.rollingpaper.logic

data class PartyStatus (
    val state: PartyState,
    val message: String? = ""
)

