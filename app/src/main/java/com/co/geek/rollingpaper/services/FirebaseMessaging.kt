package com.co.geek.rollingpaper.services

import android.app.PendingIntent
import android.content.Intent
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.co.geek.rollingpaper.R
import com.co.geek.rollingpaper.ui.lobby.LobbyViewModel.Companion.EX_PARTY_ID
import com.co.geek.rollingpaper.ui.lobby.guestlobby.GuestLobby
import com.co.geek.rollingpaper.utils.Global
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

//TODO(FIx closed notificatio behavior)
class FirebaseMessaging : FirebaseMessagingService() {

    private val TAG = FirebaseMessaging::class.java.name

    override fun onNewToken(p0: String) {
        super.onNewToken(p0)

        val id = Firebase.auth.uid

        if(id != null) {
            val reference = Firebase.database.reference
            reference
                .child("Users")
                .child(id)
                .child("token")
                .setValue(p0)
                .addOnSuccessListener {
                    Log.i(TAG,"Token updated")
                }
                .addOnFailureListener {
                    Log.e(TAG, "Token update failed")
                }
        }
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {

        Log.d(TAG, "From ${remoteMessage.from}")

        var partyId : String = ""

        remoteMessage.data.isNotEmpty().let {
            Log.d(TAG, "Message data payload: " + remoteMessage.data)

            if (it) {
                partyId = remoteMessage.data["tokenId"]!!
            } else {
                Log.i(TAG, "Something...")
            }
        }

        // Check if message contains a notification payload.
        remoteMessage.notification?.let {
            Log.d(TAG, "Message Notification Body: ${it.body}")

            val intent = Intent(this, GuestLobby::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                putExtra(EX_PARTY_ID, partyId)
            }

            val pendingIntent = PendingIntent.getActivity(
                this,
                0,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT)

            val builder = NotificationCompat.Builder(this, Global.INVITATION_CHANNEL)
                .setContentTitle("Join me!")
                .setContentText("This is a rolling paper invitation")
                .setSmallIcon(R.drawable.arrow_forwardpx)
                .setContentIntent(pendingIntent)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setAutoCancel(true)
                .build()

            with(NotificationManagerCompat.from(this)) {
                // notificationId is a unique int for each notification that you must define
                notify(0, builder)
            }
        }
    }
}