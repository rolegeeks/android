package com.co.geek.rollingpaper.`interface`

import com.co.geek.rollingpaper.logic.Character
import com.co.geek.rollingpaper.logic.Party
import retrofit2.Call
import retrofit2.http.*
import kotlin.collections.ArrayList

interface ICloudMessaging  {

    @Headers("Content-Type: application/json")
    @POST("party-sendMultiInvitation?")
    fun sendMultiInvitation(
        @Query("tokens") tokens: ArrayList<String>,
        @Query("partyToken") partyToken: String
    ) : Call<String> //or a response class

    @Headers("Content-Type: application/json")
    @POST("party-sendInvitation?")
    fun sendInvitation(
        @Query("token") token: String,
        @Query("partyToken") partyToken: String
    ) : Call<String> //or a response class

    @Headers("Content-Type: application/json")
    @POST("game-createGame?")
    fun createGame(
        @Body party: Party
    ) : Call<String>
}