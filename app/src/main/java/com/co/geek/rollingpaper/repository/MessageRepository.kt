package com.co.geek.rollingpaper.repository

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.co.geek.rollingpaper.logic.FirebaseState
import com.co.geek.rollingpaper.logic.Message
import com.co.geek.rollingpaper.logic.login.FirebaseResult
import com.co.geek.rollingpaper.utils.FirebaseConsts
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener

class MessageRepository : BaseRepository<Message>() {

    private val TAG = MessageRepository::class.java.name

    override fun getAll(
        uid: String?,
        listener: MutableLiveData<FirebaseResult>?
    ): MutableLiveData<List<Message>> {
        if(uid != null) {
            val allListener = database.child(FirebaseConsts.CHATS).child(uid)
                .addValueEventListener(object : ValueEventListener {

                    override fun onCancelled(p0: DatabaseError) {
                        Log.e(TAG, "Operation cancelled")
                        list.value = arrayListOf()
                    }

                    override fun onDataChange(p0: DataSnapshot) {
                        if(p0.exists()) {
                            val newList = arrayListOf<Message>()
                            p0.children.forEach { message ->
                                val newMessage = message.getValue(Message::class.java)!!
                                newMessage.uid = message.key!!
                                newList.add(newMessage)
                            }

                            list.value = newList
                        }
                    }

                })
            trackListener(allListener)
        }

        return list
    }

    override fun getSingle(
        uid: String?,
        listener: MutableLiveData<FirebaseResult>?
    ): MutableLiveData<Message> {
        if(uid != null) {
            val childListener = database.child(FirebaseConsts.CHATS).child(uid)
                .addChildEventListener(object : ValueEventListener, ChildEventListener {
                    override fun onCancelled(p0: DatabaseError) {
                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                    }

                    override fun onChildMoved(p0: DataSnapshot, p1: String?) {
                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                    }

                    override fun onChildChanged(p0: DataSnapshot, p1: String?) {
                        Log.i(TAG, "Child changed $p1")
                    }

                    override fun onChildAdded(p0: DataSnapshot, p1: String?) {
                        if(p0.exists()) {
                            val newMessage = p0.getValue(Message::class.java)!!
                            newMessage.uid = p0.key!!

                            single.value = newMessage
                        }
                    }

                    override fun onChildRemoved(p0: DataSnapshot) {
                        Log.i(TAG, "Chat party removed")
                    }

                    override fun onDataChange(p0: DataSnapshot) {
                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                    }

                })

            trackListener(childListener)
        }

        return single
    }

    /**
     * uid field contains partyId in this case
     */
    override fun add(item: Message, listener: MutableLiveData<FirebaseResult>?) {
        val ref = database.child(FirebaseConsts.CHATS).child(item.uid)
        val key = ref.push().key
        ref.child(key!!).setValue(item)
            .addOnSuccessListener {
                if(listener != null) {
                    listener.value = FirebaseResult(
                        state = FirebaseState.Succeeded
                    )
                }
            }
            .addOnFailureListener {
                if(listener != null) {
                    listener.value = FirebaseResult(
                        state = FirebaseState.Failed,
                        message = it.message
                    )
                }
            }
    }

    override fun delete(item: Message, listener: MutableLiveData<FirebaseResult>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}