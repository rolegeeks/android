package com.co.geek.rollingpaper.logic

import com.google.firebase.database.DataSnapshot
import java.io.Serializable

class CharacterStats() : Serializable {

    var armor = 0
    var damage = "1d6"
    var hp = 0
    var talent = "Tackle"
    var int = 0
    var dex = 0
    var str = 0

    constructor(snapshot: DataSnapshot) : this() {
        if(snapshot.exists()) {
            this.armor = (snapshot.child("armor").value.toString()).toInt()
            this.damage = snapshot.child("damage").value as String //TODO(CREATE A CLASS FOR THIS)
            this.hp = (snapshot.child("hp").value.toString()).toInt()
            this.talent = snapshot.child("talent").value.toString()
            this.int = snapshot.child("int").value.toString().toInt()
            this.dex = snapshot.child("dex").value.toString().toInt()
            this.str = snapshot.child("str").value.toString().toInt()
        } else {
            throw IllegalArgumentException("empty snapshot")
        }
    }
}