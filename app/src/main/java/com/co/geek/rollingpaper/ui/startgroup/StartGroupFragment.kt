package com.co.geek.rollingpaper.ui.startgroup

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.co.geek.rollingpaper.ui.friendselector.FriendSelector
import com.co.geek.rollingpaper.R
import com.co.geek.rollingpaper.adapters.PartyUserAdapter
import com.co.geek.rollingpaper.logic.Campaign
import com.co.geek.rollingpaper.logic.PartyUser
import com.co.geek.rollingpaper.ui.ViewModelFactory
import com.co.geek.rollingpaper.ui.lobby.LobbyViewModel.Companion.EX_PARTY_ID
import com.co.geek.rollingpaper.ui.lobby.hostlobby.HostLobby
import kotlinx.android.synthetic.main.start_group_fragment.*
import java.lang.Exception

class StartGroupFragment : Fragment() {

    private val TAG = StartGroupFragment::class.java.name

    companion object {
        fun newInstance() = StartGroupFragment()

        const val RC_ADD_FRIENDS = 1
        const val RI_FRIENDS = "friends"
    }

    private lateinit var viewModel: StartGroupViewModel
    private lateinit var _friendsAdapter: PartyUserAdapter
    private var _partyUid = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.start_group_fragment, container, false)
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.clearListeners()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        rv_friends.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        _friendsAdapter = PartyUserAdapter(context!!, arrayListOf())
        rv_friends.adapter = _friendsAdapter

        viewModel = ViewModelProvider(this, ViewModelFactory(context!!)).get(StartGroupViewModel::class.java)

        viewModel.getCampaigns().observe(viewLifecycleOwner, Observer { campaigns ->
            try {
                sp_campaigns.adapter = ArrayAdapter(
                    context!!,
                    android.R.layout.simple_spinner_dropdown_item,
                    campaigns)
            }
            catch (ex: Exception) {
                Log.e(TAG, "Something went wrong retrieving campaigns ${ex.message}")
            }
        })

        viewModel.retrieveUser().observe(viewLifecycleOwner, Observer { user ->
            try {
                if(user != null) {
                    //Disable the UI, nothing can be done if this happens
                }
            } catch (ex: Exception) {

            }
        })

        viewModel.firebaseResult.observe(viewLifecycleOwner, Observer {
            try {
                _partyUid = it.uid!!

                if(_partyUid.isNotEmpty()) {
                    Log.i(TAG, "All good, id is $_partyUid moving on")
                    val intent = Intent(context, HostLobby::class.java)
                        .putExtra(EX_PARTY_ID, _partyUid)
                    startActivity(intent)
                } else {
                    Log.e(TAG, "NO")
                }
            }
            catch (ex: Exception) {
                TODO("Log error")
            }
        })

        bt_add_select_friends.setOnClickListener{
            Log.i(TAG, "Starting activity for result")
            startActivityForResult(Intent(context, FriendSelector::class.java), RC_ADD_FRIENDS)
        }

        //we need to fill friends from activity result
    }

    @Suppress("UNCHECKED_CAST")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        try {
            if(resultCode == Activity.RESULT_OK) {
                when(requestCode) {
                    RC_ADD_FRIENDS -> _friendsAdapter.updateList(
                        data!!.getSerializableExtra(RI_FRIENDS) as List<PartyUser>)
                }
            }
            else {
                Log.e(TAG, "Something went wrong setting the income")
                //TODO: Maybe show something to the user?
            }
        }
        catch (ex: Exception) {
            Log.e(TAG, ex.message)
        }

        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.start_group, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.action_next -> {
                try {
                    //TODO(Add host id to the party object so when the app is restarted, it can be fetched)
                    viewModel.createParty(_friendsAdapter.getList(),
                        et_party_name.text.toString(),
                        sp_campaigns.selectedItem as Campaign)
                } catch (ex: Exception) {
                    Toast.makeText(context, "Error creating party", Toast.LENGTH_LONG).show()
                    Log.e(TAG, "Exception creating party, ${ex.message}")
                }
            }
        }

        return super.onOptionsItemSelected(item)
    }

}
