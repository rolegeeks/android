package com.co.geek.rollingpaper.logic

import com.google.firebase.database.DataSnapshot

class Dice() {

    var quantity = 0
    var faces = 0

    private val allowedSizes = arrayOf(4, 6, 8, 10, 12, 20, 100)

    constructor(dataSnapshot: DataSnapshot) : this() {
        with(dataSnapshot) {
            if(exists()) {
                quantity = this.child("quantity").value.toString().toInt()
                faces = this.child("faces").value.toString().toInt()
                if(!allowedSizes.contains(faces)) {
                    throw IllegalArgumentException("Unsupported dice size")
                }
            }
        }
    }

    fun roll() : Int {
        var output = 0
        //percentile dice
        if(faces == 100) {
            val firstRoll = rollDice(9)
            val secondRoll = rollDice(9)
            output = "${firstRoll}${secondRoll}".toInt()
        } else {
            output = rollDice(faces)
        }

        return output
    }

    private fun rollDice(size: Int) : Int {
        val number = Math.random() * size
        return number.toInt()
    }

}