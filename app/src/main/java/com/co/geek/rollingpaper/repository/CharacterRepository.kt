package com.co.geek.rollingpaper.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.co.geek.rollingpaper.logic.Character
import com.co.geek.rollingpaper.logic.login.FirebaseResult
import com.co.geek.rollingpaper.utils.FirebaseConsts
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import java.lang.Exception
import java.lang.IllegalArgumentException

class CharacterRepository : BaseRepository<Character>() {

    private val TAG = CharacterRepository::class.java.name
    private var characterList = listOf<Character>()

    override fun getAll(
        uid: String?,
        listener: MutableLiveData<FirebaseResult>?
    ): LiveData<List<Character>> {

        database.child(FirebaseConsts.CHARACTERS)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {
                    //Find a way to lock the commands and set a timer to try again later
                }

                override fun onDataChange(p0: DataSnapshot) {
                    Log.d(TAG, "Got all characters!")
                    characterList = if(p0.exists()) {
                        val arrayList = arrayListOf<Character>()
                        p0.children.forEach {charSnapshot ->
                            arrayList.add(Character(charSnapshot))
                        }
                        arrayList
                    } else {
                        listOf()
                    }

                    if(!uid.isNullOrEmpty()) {
                        val listener = database
                            .child(FirebaseConsts.USERS)
                            .child(Firebase.auth.uid!!)
                            .child(FirebaseConsts.USER_CHARACTERS)
                            .addValueEventListener(object : ValueEventListener {

                                override fun onCancelled(p0: DatabaseError) {
                                    list.value = null
                                }

                                override fun onDataChange(p0: DataSnapshot) {
                                    Log.d(TAG, "Getting this ones!")
                                    val newCharacters = arrayListOf<Character>()
                                    p0.children.forEach { character ->
                                        val characterId = character.value.toString()
                                        newCharacters.add(characterList.find { char -> char.uid == characterId }!!)
                                    }

                                    list.value = newCharacters
                                }

                            })
                        trackListener(listener)
                    } else {
                        list.value = characterList
                    }
                }

            })

        return list
    }

    override fun add(item: Character, listener: MutableLiveData<FirebaseResult>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun delete(item: Character, listener: MutableLiveData<FirebaseResult>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getSingle(
        uid: String?,
        listener: MutableLiveData<FirebaseResult>?
    ): LiveData<Character> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}