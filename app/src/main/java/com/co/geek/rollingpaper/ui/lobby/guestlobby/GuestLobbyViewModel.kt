package com.co.geek.rollingpaper.ui.lobby.guestlobby

import com.co.geek.rollingpaper.logic.Character
import com.co.geek.rollingpaper.ui.lobby.LobbyViewModel

class GuestLobbyViewModel : LobbyViewModel() {

    override fun updateStatus(partyId: String) {
        partyRepository.updateStatus(partyId, _updateStatus)
    }

    override fun deleteParty() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun startGroup() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun updateJoinedStatus(partyId: String, joined: Boolean) {
        partyRepository.updateJoinedStatus(partyId, joined, _joinedStatus)
    }
}
