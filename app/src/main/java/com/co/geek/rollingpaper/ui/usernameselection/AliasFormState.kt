package com.co.geek.rollingpaper.ui.usernameselection

data class AliasFormState(
    val state: AliasState? = null,
    val message: String? = null,
    val canWork: Boolean = false
)

enum class AliasState {
    Invalid,
    Valid,
    Successful,
    Loaded,
    Failed
}