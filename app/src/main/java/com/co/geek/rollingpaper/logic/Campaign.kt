package com.co.geek.rollingpaper.logic

import com.google.firebase.database.DataSnapshot
import java.io.Serializable

class Campaign() : Serializable {
    var uid: String = ""
    var name: String = ""
    var length: ArrayList<Int> = arrayListOf()
    var description: String = ""
    var characterList: ArrayList<Character> = arrayListOf()

    constructor(snapshot: DataSnapshot) : this() {
        this.description = snapshot.child("description").value as String
        this.length = snapshot.child("length").value as ArrayList<Int>
        this.name = snapshot.child("name").value as String
        if(snapshot.hasChild("uid")) {
            this.uid = snapshot.child("uid").value as String
        }
        else {
            this.uid = snapshot.key!!
        }
    }

    override fun toString(): String {
        return "${name} - (${description})"
    }
}