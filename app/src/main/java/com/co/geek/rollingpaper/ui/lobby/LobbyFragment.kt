package com.co.geek.rollingpaper.ui.lobby

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.co.geek.rollingpaper.adapters.ChatAdapter
import com.co.geek.rollingpaper.adapters.LobbyAdapter
import com.co.geek.rollingpaper.logic.FirebaseState
import com.co.geek.rollingpaper.ui.characterselection.CharacterSelection
import com.co.geek.rollingpaper.ui.game.BaseGame
import com.co.geek.rollingpaper.ui.game.GameHost
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.lobby.*
import kotlinx.android.synthetic.main.lobby_chat.*

abstract class LobbyFragment : Fragment() {

    private val TAG = LobbyFragment::class.java.name

    protected var partyId: String = ""
    protected var alias: String = ""

    protected lateinit var joinedAdapter: LobbyAdapter
    protected lateinit var lobbyViewModel: LobbyViewModel

    private lateinit var chatAdapter: ChatAdapter

    var backPressed = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            partyId = it.getString(ARG_PARTY_ID)!!
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        lobbyViewModel.clearListeners()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        Log.d(TAG, "Entered in base method!")
        super.onActivityCreated(savedInstanceState)

        rv_joined_users.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        joinedAdapter = LobbyAdapter(context!!, arrayListOf(), arrayListOf())
        rv_joined_users.adapter = joinedAdapter

        val chatLayoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, true)
        rv_chat.layoutManager = chatLayoutManager
        chatAdapter = ChatAdapter(arrayListOf())
        rv_chat.adapter = chatAdapter

        lobbyViewModel.getParty(partyId).observe(viewLifecycleOwner, Observer { party ->
            try {
                campaign_name.text = party.name
                alias = party.members.firstOrNull { x -> x.uid == Firebase.auth.uid }!!.alias
                joinedAdapter.updateUsers(party.members)

                if(party.started) {
                    beginGame()
                }
            }
            catch (ex: Exception) {
                Log.e(TAG, "Something went wrong updating party info ${ex.message}")
            }
        })

        lobbyViewModel.getCharacters().observe(viewLifecycleOwner, Observer {
            val characters = it ?: return@Observer

            try {
                joinedAdapter.updateCharacters(characters)
            } catch (ex: Exception) {
                Log.e(TAG, "Something went wrong updating character list ${ex.message}")
            }
        })

        lobbyViewModel.getChatMessages(partyId).observe(viewLifecycleOwner, Observer {
            val message = it ?: return@Observer

            chatAdapter.addMessage(message)
        })

        lobbyViewModel.characterSelectionStatus.observe(viewLifecycleOwner, Observer {
            val characterSelectionStatus = it ?: return@Observer

            when(characterSelectionStatus.state) {
                FirebaseState.Nothing -> TODO()
                FirebaseState.Succeeded -> {
                    //really nothing... Should be updated on its own
                }
                FirebaseState.Cancelled, FirebaseState.Failed -> {
                    //For now do a Toast
                    Toast.makeText(context, characterSelectionStatus.message, Toast.LENGTH_LONG)
                        .show()
                }
            }
        })

        lobbyViewModel.sendState.observe(viewLifecycleOwner, Observer {
            val sendState = it ?: return@Observer

            when(sendState.state) {
                FirebaseState.Succeeded -> {
                    et_chat.text.clear()
                }
                FirebaseState.Failed -> {
                    Log.e(TAG, "Failed state received: ${sendState.message}")
                    Toast.makeText(context, sendState.message, Toast.LENGTH_LONG).show()
                }
                FirebaseState.Nothing, FirebaseState.Cancelled, FirebaseState.Removed -> {
                    Log.w(TAG, "Received unsupported state")
                }
            }
        })

        bt_send_chat_message.setOnClickListener {
            lobbyViewModel.sendChatMessage(partyId, et_chat.text.toString(), alias)
        }

        bt_change_character.setOnClickListener {
            val intent = Intent(context, CharacterSelection::class.java)
            startActivityForResult(intent, RC_CHARACTER_SELECT)
        }
    }

    abstract fun beginGame()

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        try {
            if(resultCode == Activity.RESULT_OK) {
                when(requestCode) {
                    RC_CHARACTER_SELECT -> {
                        Log.i(TAG, "Character selected!")
                        if(data!!.extras != null) {
                            lobbyViewModel
                                .updateCharacterSelection(
                                    partyId,
                                    data.getSerializableExtra(RI_SELECTED) as String)
                        }
                    }
                    else -> {
                        Log.i(TAG, "what?!")
                    }
                }
            } else {
                Log.e(TAG, "something broke")
            }
        } catch (ex: Exception) {

        }
    }

    companion object {
        const val RC_CHARACTER_SELECT = 0
        const val RI_SELECTED = "character"
        const val ARG_PARTY_ID = "party_id"
    }
}