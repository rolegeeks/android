package com.co.geek.rollingpaper.ui.lobby.hostlobby

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import com.co.geek.rollingpaper.R
import com.co.geek.rollingpaper.ui.lobby.LobbyViewModel.Companion.EX_PARTY_ID
import com.co.geek.rollingpaper.ui.lobby.guestlobby.GuestLobbyFragment
import com.co.geek.rollingpaper.utils.ConfirmationDialogFragment

class HostLobby : AppCompatActivity(), ConfirmationDialogFragment.ConfirmationDialogListener {

    private lateinit var hostLobbyFragment : HostLobbyFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.host_lobby_activity)
        if (savedInstanceState == null) {
            hostLobbyFragment = HostLobbyFragment.
                newInstance(intent.extras[EX_PARTY_ID] as String)

            supportFragmentManager.beginTransaction()
                .replace(R.id.container, hostLobbyFragment)
                .commitNow()
        }
    }

    override fun onBackPressed() {

        if(hostLobbyFragment.backPressed) {
            hostLobbyFragment.backPressed = false
            super.onBackPressed()
        } else {
            val dialog = ConfirmationDialogFragment("Are you sure you want to dismiss the party?");
            dialog.show(supportFragmentManager, "ConfirmationDialogFragment")
        }
    }

    override fun onDialogPositiveClick(dialog: DialogFragment) {
        //we need to ask the fragment to delete the party
        val fragment = supportFragmentManager.fragments[0] as HostLobbyFragment
        fragment.deleteParty()
        hostLobbyFragment.backPressed = true
    }

    override fun onDialogNegativeClick(dialog: DialogFragment) {
        hostLobbyFragment.backPressed = false
    }

    fun goBack() {
        hostLobbyFragment.backPressed = true
        onBackPressed()
    }
}
