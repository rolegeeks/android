package com.co.geek.rollingpaper.logic

import com.google.firebase.database.DataSnapshot
import java.io.Serializable

class Game() : FirebaseItem(), Serializable {

    var name: String = ""
    var description: String = ""
    var isRunning: Boolean = false
    var boxes: ArrayList<Box> = ArrayList()
    var members = arrayListOf<GameMember>()
    var progress = GameProgress()

    constructor(dataSnapshot: DataSnapshot) : this() {
        with(dataSnapshot) {
            if(exists()) {
                name = child("name").value.toString()
                child("members").children.forEach { member ->
                    members.add(GameMember(member))
                }
                progress = GameProgress(child("progress"))
                description = child("description").value.toString()

                child("boxes").children.forEach { box ->
                    boxes.add(Box(box))
                }
            }
        }
    }

    constructor(key: String) : this() {
        this.uid = key
    }

    override fun toFirebaseMap(): HashMap<String, Any> {
        return hashMapOf(
            "name" to name,
            "description" to description
        )
    }
}
