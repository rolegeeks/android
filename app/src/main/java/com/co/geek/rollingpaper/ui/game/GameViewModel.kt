package com.co.geek.rollingpaper.ui.game

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.co.geek.rollingpaper.R
import com.co.geek.rollingpaper.logic.*
import com.co.geek.rollingpaper.logic.Game
import com.co.geek.rollingpaper.repository.GameRepository
import com.co.geek.rollingpaper.ui.BaseViewModel
import kotlinx.android.synthetic.main.game_action.view.*
import kotlinx.android.synthetic.main.portrait_item.view.*

class GameViewModel(private var context: Context, gameId: String): BaseViewModel() {

    private val gameMemberFrames = HashMap<String, View>()

    private val TAG = GameViewModel::class.java.name.toString()

    private var _gameStatus = MutableLiveData<GameStatus>()
    var gameStatus = _gameStatus

    private var gameRepository: GameRepository = GameRepository(_gameStatus, gameId)

    fun getGame(uid: String) : LiveData<Game> {
        return gameRepository.getSingle(uid)
    }

    fun renderActionFrame(layout: LinearLayout, container: ViewGroup, options: ArrayList<BoxOption>) {
        layout.removeAllViews()
        options.forEach {opt ->
            val inflater = LayoutInflater.from(context)
            val view = inflater.inflate(R.layout.game_action, container, false)
            view.layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT,
                1f)
            view.bt_game_action.text = opt.narrative
            view.bt_game_action.setOnClickListener {sendAction(opt)}
            layout.addView(view)
        }
    }

    fun renderGameMembers(layout: LinearLayout, container: ViewGroup, gameMembers: List<GameMember>) {
        gameMembers.forEach {

            val key = gameMemberFrames[it.uid]

            //Go ahead an only update values
            if(key != null) {
                key.hp_bar.progress = it.character.stats.hp

                //TODO(Update image using Glide or whatever)
            } else {
                val inflater = LayoutInflater.from(context)
                val view = inflater.inflate(R.layout.portrait_item, container, false)
                view.layoutParams = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    1f)
                gameMemberFrames[it.uid] = view
                layout.addView(view)
            }
        }
    }

    private fun sendAction(opt: BoxOption) {
        Log.d(TAG, "Clicked action with id ${opt.uid}")
        //First we need to calculate the action and build the object we're going to post
        calculateOptionTaken(opt)
        gameRepository.setActionTaken(opt)
    }

    private fun calculateOptionTaken(option: BoxOption) {

    }

    override fun clearListeners() {
        gameRepository.cleanUpListeners()
    }

    fun getMemberFrames(gameId: String): LiveData<List<GameMember>> {
        TODO("Not yet implemented")
    }
}