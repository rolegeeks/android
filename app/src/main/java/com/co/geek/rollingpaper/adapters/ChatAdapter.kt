package com.co.geek.rollingpaper.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.co.geek.rollingpaper.R
import com.co.geek.rollingpaper.logic.Message
import kotlinx.android.synthetic.main.message_model_adapter.view.*
import java.lang.Exception

class ChatAdapter(private var messageList: ArrayList<Message>)
    : RecyclerView.Adapter<ChatAdapter.ViewHolder>() {

    private val TAG = ChatAdapter::class.java.name

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val message: TextView = itemView.tv_message
        val time: TextView = itemView.tv_time
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.message_model_adapter, parent, false))
    }

    override fun getItemCount(): Int {
        return messageList.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        try {
            with(holder) {
                val currentMessage = messageList[position]
                message.text = "${currentMessage.displayName}: ${currentMessage.message}"
                time.text = currentMessage.timestamp.toString()
            }
        } catch (ex: Exception) {

        }
    }

    fun addMessage(newMessage: Message) {
        messageList.add(0, newMessage)
        notifyDataSetChanged()
    }
}