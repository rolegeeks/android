package com.co.geek.rollingpaper.logic

enum class BoxType {
    Undefined,
    Start,
    End,
    Road
}