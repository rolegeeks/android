package com.co.geek.rollingpaper.logic

import java.io.Serializable

class UserSettings : User() , Serializable {
    var friends: ArrayList<String> = arrayListOf()
}