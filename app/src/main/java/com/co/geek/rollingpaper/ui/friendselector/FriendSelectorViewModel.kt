package com.co.geek.rollingpaper.ui.friendselector

import android.content.Context
import androidx.lifecycle.LiveData
import com.co.geek.rollingpaper.logic.Friend
import com.co.geek.rollingpaper.repository.UserRepository
import com.co.geek.rollingpaper.ui.BaseViewModel

class FriendSelectorViewModel(context: Context) : BaseViewModel() {

    private var friendRepository: UserRepository = UserRepository(context)

    fun getFriends() : LiveData<List<Friend>> {
        return friendRepository.getFriends()
    }

    override fun clearListeners() {
        friendRepository.cleanUpListeners()
    }
}
