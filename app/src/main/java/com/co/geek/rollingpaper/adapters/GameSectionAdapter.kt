package com.co.geek.rollingpaper.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.co.geek.rollingpaper.R
import com.co.geek.rollingpaper.logic.PendingGame
import kotlinx.android.synthetic.main.game_section_adapter.view.*

class GameSectionAdapter(var sectionedGames: HashMap<Int, ArrayList<PendingGame>>) :
    RecyclerView.Adapter<GameSectionAdapter.ViewHolder>() {

    private var TAG = GameSectionAdapter::class.java.name
    private var pendingGameAdapter: PendingGameAdapter = PendingGameAdapter(arrayListOf())

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val section: TextView = itemView.game_section
        val recyclerView: RecyclerView = itemView.game_list
    }

    override fun getItemCount(): Int {
        return sectionedGames.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val games = sectionedGames[position]
        with(holder) {
            section.text = if (position == 0) "Pending" else "Started"

            if(recyclerView.adapter == null) {
                recyclerView.layoutManager =
                    LinearLayoutManager(itemView.context, RecyclerView.VERTICAL, false)
                recyclerView.adapter = PendingGameAdapter(games!!)
            } else {
                pendingGameAdapter.updateGames(games!!)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater
            .from(parent.context)
            .inflate(R.layout.game_section_adapter, parent, false))
    }

    fun formatGames(newGames: List<PendingGame>) {
        try {
            val formattedGames = HashMap<Int, ArrayList<PendingGame>>()
            formattedGames[0] = arrayListOf()
            formattedGames[1] = arrayListOf()

            newGames.forEach { game ->
                val index = if(game.isRunning) {
                    1
                } else {
                    0
                }
                formattedGames[index]?.add(game)
            }
            sectionedGames.clear()
            sectionedGames = formattedGames
            notifyDataSetChanged()
        } catch (ex: Exception) {
            Log.e(TAG, ex.message)
            throw ex
        }
    }
}