package com.co.geek.rollingpaper.ui.friends

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.co.geek.rollingpaper.R
import com.co.geek.rollingpaper.adapters.FriendListAdapter
import com.co.geek.rollingpaper.logic.FirebaseState
import com.co.geek.rollingpaper.ui.ViewModelFactory
import kotlinx.android.synthetic.main.activity_friends.*

class Friends : AppCompatActivity() {

    private val TAG = Friends::class.java.name

    private lateinit var friendViewModel: FriendsViewModel
    private lateinit var friendsAdapter: FriendListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_friends)

        //Setting initial UI State
        updateUI(false)

        rv_friends.layoutManager = LinearLayoutManager(applicationContext, RecyclerView.VERTICAL, false)
        friendsAdapter = FriendListAdapter(applicationContext, arrayListOf())
        rv_friends.adapter = friendsAdapter

        friendViewModel = ViewModelProvider(this, ViewModelFactory(applicationContext)).get(FriendsViewModel::class.java)

        friendViewModel.getFriends().observe(this, Observer {
            val friendList = it ?: return@Observer

            friendsAdapter.updateList(friendList)
            //create an adapter and populate friend list with special commands and everything
        })

        friendViewModel.searchState.observe(this, Observer {
            val searchState = it ?: return@Observer

            //here we will handle the result of searching/adding a friend
            when(searchState.state) {
                FirebaseState.Nothing -> TODO()
                FirebaseState.Succeeded -> {
                    tv_result.text = searchState.message
                    Toast.makeText(applicationContext, searchState.message, Toast.LENGTH_SHORT)
                        .show()
                }
                FirebaseState.Cancelled, FirebaseState.Failed -> {
                    updateUI(searchState.enableUI)
                    tv_result.text = searchState.message
                    Toast.makeText(applicationContext, searchState.message, Toast.LENGTH_SHORT)
                        .show()
                }
            }
        })

        friendViewModel.allUsersState.observe(this, Observer {
            val usersState = it ?: return@Observer

            //here we need to enable/disable UI accordingly
            when(usersState.state) {
                FirebaseState.Nothing -> TODO()
                FirebaseState.Succeeded -> {
                    updateUI(usersState.enableUI)
                }
                FirebaseState.Cancelled, FirebaseState.Failed -> updateUI(usersState.enableUI)
            }
        })

        friendViewModel.getUsers()

        bt_search.setOnClickListener {
            friendViewModel.addFriend(et_friend.text.toString())
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        friendViewModel.clearListeners()
    }

    private fun updateUI(state: Boolean) {
        bt_search.isEnabled = state
        et_friend.isEnabled = state
    }
}
