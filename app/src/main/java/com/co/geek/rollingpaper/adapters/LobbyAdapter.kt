package com.co.geek.rollingpaper.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.co.geek.rollingpaper.R
import com.co.geek.rollingpaper.logic.Character
import com.co.geek.rollingpaper.logic.PartyUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.group_waiting_model_adapter.view.*
import java.lang.Exception

class LobbyAdapter(private var context: Context,
                   private var users: ArrayList<PartyUser>,
                   private var characters: List<Character>)
    : RecyclerView.Adapter<LobbyAdapter.ViewHolder>() {

    private val TAG = LobbyAdapter::class.java.name

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val userName: TextView = itemView.tv_user_name
        val userClass: TextView = itemView.tv_class
        val cardView: CardView = itemView.cv_user
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater
            .from(parent.context)
            .inflate(R.layout.group_waiting_model_adapter, parent, false))
    }

    override fun getItemCount(): Int {
        return users.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        try {
            with(holder) {

                with(users[position]) {

                    userName.text = alias

                    if(characterId.isEmpty()) {
                        userClass.text = "No selection"
                    }
                    else {
                        userClass.text = characters.firstOrNull{ p -> p.uid == characterId }!!.alias
                    }

                    if(inParty) {
                        if(ready || host) {
                            cardView.setBackgroundColor(ContextCompat.getColor(context, R.color.green))
                        }
                        else {
                            cardView.setBackgroundColor(ContextCompat.getColor(context, R.color.white))
                        }
                    }
                    else {
                        cardView.setBackgroundColor(ContextCompat.getColor(context, R.color.gray))
                    }
                }
            }
        }
        catch (ex: Exception) {
            Log.e(TAG, ex.message)
        }

    }

    fun updateUsers(newUsers: List<PartyUser>) {
        users = ArrayList(newUsers)
        notifyDataSetChanged()
    }

    fun updateCharacters(newCharacters: List<Character>) {
        characters = newCharacters
        notifyDataSetChanged()
    }

    fun updateStatus() : Boolean {
        var output = false
        users.forEach { user ->
            if(user.uid == Firebase.auth.uid) {
                output = user.ready
            }
        }
        return output
    }
}