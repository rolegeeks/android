package com.co.geek.rollingpaper.repository

import android.os.Handler
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.co.geek.rollingpaper.logic.login.FirebaseResult
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.guest_lobby_fragment.view.*
import java.lang.Exception

//TODO("Add destruction logic for listeners, maybe an array of listeners")
abstract class BaseRepository<T> {

    private var TAG = BaseRepository::class.java.name
    private var valueEventListeners: ArrayList<ValueEventListener> = arrayListOf()
    private var childEventListeners: ArrayList<ChildEventListener> = arrayListOf()
    protected var database = Firebase.database.reference

    protected var single: MutableLiveData<T> = MutableLiveData()
    protected var list: MutableLiveData<List<T>> = MutableLiveData()

    fun getUnit() : T {
        return single.value!!
    }

    fun getList(): List<T> {
        return list.value!!
    }

    abstract fun getAll(uid:String? = null, listener: MutableLiveData<FirebaseResult>? = null)
            : LiveData<List<T>>
    abstract fun getSingle(uid: String? = null, listener: MutableLiveData<FirebaseResult>? = null)
            : LiveData<T>
    abstract fun add(item: T, listener: MutableLiveData<FirebaseResult>? = null)
    abstract fun delete(item: T, listener: MutableLiveData<FirebaseResult>? = null)

    fun cleanUpListeners() {
        try {
            valueEventListeners.forEach {
                database.removeEventListener(it)
            }
            childEventListeners.forEach {
                database.removeEventListener(it)
            }
        } catch (ex: Exception) {
            Log.e(TAG, "Dangerously leaking memory! ex: ${ex.message}")
        }

    }

    fun trackListener(listener: ValueEventListener) {
        valueEventListeners.add(listener)
    }

    fun trackListener(listener: ChildEventListener) {
        childEventListeners.add(listener)
    }
}