package com.co.geek.rollingpaper.logic

import java.io.Serializable

class PartyUser : User(), Serializable {
    var inParty: Boolean = false
    var ready: Boolean = false
    var characterId: String = ""
    var host: Boolean = false
    var chatColor: Int = 0


    override fun toFirebaseMap(): HashMap<String, Any> {
        return hashMapOf(
            uid to uid
        )
    }
}