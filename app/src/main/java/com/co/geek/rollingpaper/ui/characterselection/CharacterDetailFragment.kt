package com.co.geek.rollingpaper.ui.characterselection

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.co.geek.rollingpaper.R
import com.co.geek.rollingpaper.logic.Character
import com.co.geek.rollingpaper.ui.lobby.LobbyFragment
import kotlinx.android.synthetic.main.character_detail.*

private const val ARG_CHARACTER = "Character"

class CharacterDetailFragment : Fragment() {

    private val TAG = CharacterDetailFragment::class.java.name
    private var character: Character? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            character = it.getSerializable(ARG_CHARACTER) as Character
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.character_detail, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        character?.let {
            tv_alias.text = it.alias
            tv_character_class.text = it.characterClass
            tv_armor.text = it.stats.armor.toString()
            tv_damage.text = it.stats.damage
            tv_hp.text = it.stats.hp.toString()
            tv_talent.text = it.stats.talent
        }

        bt_select_character.setOnClickListener {
            val returnIntent = Intent()
            returnIntent.putExtra(LobbyFragment.RI_SELECTED, character!!.uid)
            activity!!.setResult(Activity.RESULT_OK, returnIntent)
            activity!!.finish()
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(character: Character) =
            CharacterDetailFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(ARG_CHARACTER, character)
                }
            }
    }
}
