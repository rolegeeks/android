package com.co.geek.rollingpaper.ui.game

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.co.geek.rollingpaper.R
import com.co.geek.rollingpaper.logic.ProgressStatus
import com.co.geek.rollingpaper.ui.ViewModelFactory
import com.co.geek.rollingpaper.utils.ConfirmationDialogFragment
import kotlinx.android.synthetic.main.activity_game.*

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
open class BaseGame : AppCompatActivity(), ConfirmationDialogFragment.ConfirmationDialogListener {
    private var backPressed = false
    private val mHideHandler = Handler()
    private val mHidePart2Runnable = Runnable {
        // Delayed removal of status and navigation bar

        // Note that some of these constants are new as of API 16 (Jelly Bean)
        // and API 19 (KitKat). It is safe to use them, as they are inlined
        // at compile-time and do nothing on earlier devices.
        game_content.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LOW_PROFILE or
                    View.SYSTEM_UI_FLAG_FULLSCREEN or
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                    View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY or
                    View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
                    View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
    }
    private val mShowPart2Runnable = Runnable {
        // Delayed display of UI elements
        supportActionBar?.show()
    }
    private var mVisible: Boolean = false
    private val mHideRunnable = Runnable { hide() }

    protected lateinit var gameViewModel: GameViewModel
    protected var gameId = ""
    protected var currentBoxId = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_game)

        if(savedInstanceState == null) {

            if(intent.extras != null) {
                gameId = intent.getStringExtra(PARTY_ID)
            }
        }

        gameViewModel = ViewModelProvider(this, ViewModelFactory(applicationContext, gameId))
            .get(GameViewModel::class.java)


        gameViewModel.getGame(gameId).observe(this, Observer {
            val game = it ?: return@Observer

            //Render or update member frames
            gameViewModel.renderGameMembers(member_portraits, game_content, game.members)
        })

        gameViewModel.gameStatus.observe(this, Observer {
            val gameStatus = it ?: return@Observer

            when(gameStatus.progressState) {
                ProgressStatus.WaitingForPlayers -> {
                    Toast.makeText(applicationContext, gameStatus.message, Toast.LENGTH_LONG)
                        .show()
                }
                ProgressStatus.Synced -> {
                    //enable ui
                }
                ProgressStatus.Advance -> {
                    //Here we need to ask the viewmodel to render the box
                    val box = gameStatus.currentBox

                    narration.text = box?.narrative

                    gameViewModel.renderActionFrame(action_frame, game_content, box?.options!!)
                }
                ProgressStatus.ActionTaken -> {
                    //Maybe turn some progressbar to let everyone know an action was taken
                }
                ProgressStatus.Undefined -> TODO()
            }
        })

        game_back.setOnClickListener {
            onBackPressed()
        }

        mVisible = true

        // Upon interacting with UI controls, delay any scheduled hide()
        // operations to prevent the jarring behavior of controls going away
        // while interacting with the UI.
    }


    override fun onBackPressed() {

        if(backPressed) {
            backPressed = false
            super.onBackPressed()
        } else {
            val dialog = ConfirmationDialogFragment("Are you sure you want to exit the game?");
            dialog.show(supportFragmentManager, "ConfirmationDialogFragment")
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        gameViewModel.clearListeners()
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        delayedHide(300)
    }

    override fun onResume() {
        super.onResume()
        delayedHide(300)
    }

    private fun hide() {
        // Hide UI first
        supportActionBar?.hide()
        mVisible = false

        // Schedule a runnable to remove the status and navigation bar after a delay
        mHideHandler.removeCallbacks(mShowPart2Runnable)
        mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY.toLong())
    }

    /**
     * Schedules a call to hide() in [delayMillis], canceling any
     * previously scheduled calls.
     */
    private fun delayedHide(delayMillis: Int) {
        mHideHandler.removeCallbacks(mHideRunnable)
        mHideHandler.postDelayed(mHideRunnable, delayMillis.toLong())
    }

    companion object {
        /**
         * Whether or not the system UI should be auto-hidden after
         * [AUTO_HIDE_DELAY_MILLIS] milliseconds.
         */
        private val AUTO_HIDE = true

        /**
         * If [AUTO_HIDE] is set, the number of milliseconds to wait after
         * user interaction before hiding the system UI.
         */
        private val AUTO_HIDE_DELAY_MILLIS = 3000

        /**
         * Some older devices needs a small delay between UI widget updates
         * and a change of the status and navigation bar.
         */
        private val UI_ANIMATION_DELAY = 300

        const val PARTY_ID = "party"
    }

    override fun onDialogPositiveClick(dialog: DialogFragment) {
        backPressed = true
        onBackPressed()
    }

    override fun onDialogNegativeClick(dialog: DialogFragment) {
        backPressed = false
        delayedHide(200)
    }
}
