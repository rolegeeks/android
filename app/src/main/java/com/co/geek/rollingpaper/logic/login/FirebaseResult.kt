package com.co.geek.rollingpaper.logic.login

import com.co.geek.rollingpaper.logic.FirebaseState

data class FirebaseResult(
    val state: FirebaseState,
    val message: String? = "",
    val enableUI: Boolean = true,
    val uid: String? = ""
)