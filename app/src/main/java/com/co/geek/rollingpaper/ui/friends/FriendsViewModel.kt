package com.co.geek.rollingpaper.ui.friends

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.co.geek.rollingpaper.logic.FirebaseState
import com.co.geek.rollingpaper.logic.Friend
import com.co.geek.rollingpaper.logic.User
import com.co.geek.rollingpaper.logic.login.FirebaseResult
import com.co.geek.rollingpaper.repository.UserRepository
import com.co.geek.rollingpaper.ui.BaseViewModel
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import java.lang.Exception
import java.util.*

class FriendsViewModel(context: Context) : BaseViewModel() {

    private val userRepository: UserRepository = UserRepository(context)

    private var _searchResult: MutableLiveData<FirebaseResult> = MutableLiveData()
    var searchState: LiveData<FirebaseResult> = _searchResult

    private var _allUsersResult: MutableLiveData<FirebaseResult> = MutableLiveData()
    var allUsersState: LiveData<FirebaseResult> = _allUsersResult

    private var users: MutableLiveData<List<User>> = MutableLiveData()

    fun getFriends() : LiveData<List<Friend>> {
        return userRepository.getFriends()
    }

    fun getUsers() {
        users = userRepository.getAll(listener =  _allUsersResult)
    }

    fun addFriend(query: String) {
        try {

            if(users.value != null) {
                val user = users.value!!.firstOrNull() {
                    it.alias.toLowerCase(Locale.ROOT).contentEquals(query.toLowerCase(Locale.ROOT))
                }

                //Then we can't add it
                if(user == null) {
                    _searchResult.value = FirebaseResult(
                        state = FirebaseState.Failed,
                        message = "User could not be found",
                        enableUI = true
                    )
                } else {

                    if(user.uid == Firebase.auth.uid) {
                        _searchResult.value = FirebaseResult(
                            state = FirebaseState.Failed,
                            message = "Cannot add yourself",
                            enableUI = true
                        )
                    } else {
                        userRepository.addFriend(user.uid, user.alias, _searchResult)
                    }
                }
            } else {
                _searchResult.value = FirebaseResult(
                    state = FirebaseState.Failed,
                    message = "Could not find user",
                    enableUI = true
                )
            }
        } catch (ex: Exception) {
            _searchResult.value = FirebaseResult(
                state = FirebaseState.Failed,
                message = "Could not find user, ${ex.message}",
                enableUI = true
            )
        }
    }

    /**
     * TODO("Need to include checks here about what is written")
     */
    fun validateUserQuery(query: String) {

    }

    override fun clearListeners() {
        userRepository.cleanUpListeners()
    }
}